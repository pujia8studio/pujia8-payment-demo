﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com.moogle.gameworks;

public class PurchaseExample : MonoBehaviour
{
    Button purchaseConsumableButton;
    Button purchaseNonConsumableButton;
    Button getPriceButton;
    Button getRestoreButton;
    Button deletePrefsButton;
    Text outputMsgWindow;
    Text purchaseConsumableButtonText;
    Text purchaseNonConsumableButtonText;

    string pay_item_0 = "test_pay_0";
    string pay_item_1 = "test_pay_1";

    void InitComponents()
    {
        //init all ui components
        deletePrefsButton = transform.Find("BasePanel/ButtonDeletePrefs").GetComponent<Button>();
        purchaseConsumableButton = transform.Find("BasePanel/Items/UIItem_0/Button").GetComponent<Button>();
        purchaseNonConsumableButton = transform.Find("BasePanel/Items/UIItem_1/Button").GetComponent<Button>();
        purchaseConsumableButtonText = transform.Find("BasePanel/Items/UIItem_0/Button/Text").GetComponent<Text>();
        purchaseNonConsumableButtonText = transform.Find("BasePanel/Items/UIItem_1/Button/Text").GetComponent<Text>();
        getPriceButton = transform.Find("BasePanel/ButtonGetPrice").GetComponent<Button>();
        getRestoreButton = transform.Find("BasePanel/ButtonRestore").GetComponent<Button>();
        outputMsgWindow = transform.Find("BasePanel/OutputMessage").GetComponent<Text>();
        //add listeners
        getPriceButton.onClick.AddListener(() => { OnGetPriceButton_Clicked(); });
        purchaseConsumableButton.onClick.AddListener(() => { OnPurchaseConsumableButton_Clicked(); });
        purchaseNonConsumableButton.onClick.AddListener(() => { OnPurchaseNonConsumableButton_Clicked(); });
        deletePrefsButton.onClick.AddListener(() => { RemoveAllPlayerPrefs(); });
        getRestoreButton.onClick.AddListener(() => { OnGetRestoreButton_Clicked(); });
    }


    // Use this for initialization
    void Start()
    {
        InitComponents();
        InitAllSystem();
    }


    void InitAllSystem()
    {
        //simple init analystic system for TapDb
        //site: https://www.tapdb.com
        //TapDb service needs an APPID, 
        //See Configs/iOSConfig/TapdbConf.json
        //And Plugins/Android/gameworks_release/assets/TapdbConf.json
        //Change "TapDB_Tracker_AppID" .

        InitAnalyticSystem();

        //simple init payment system();
        //We also support iOS IAP system with UnityIAP plugins.
        //Please Install UnityIAP from AssetStore if you need iOS IAP with Pujia8 IAP Manager.
        //See : GameworkPaymentIOSMgr.cs and PaymentIOSListener.cs

        GameworkPaymentMgr.Instance.Init(() => {
            GetItemPriceTable();
        });
        //add products
        GameworkPaymentMgr.Instance.AddProducts(
            new GameStoreProduct(pay_item_0, "Consumable Item 0", "Consumable Item 0", E_GameStoreConsumable.Consumable)
        );
        GameworkPaymentMgr.Instance.AddProducts(
            new GameStoreProduct(pay_item_1, "Non-Consumable Item 1", "Consumable Item 1",
                E_GameStoreConsumable.NonConsumable)
        );
        //Enable Sandbox Test Mode (Must be diabled in release builds).
        
        GameworkPaymentMgr.Instance.EnablePaymentSandbox(true);
        
        //try get price table and show on UI Panel.
        GetItemPriceTable();
    }

    void InitAnalyticSystem()
    {
        GameworksTrackerMgr.Instance.Init();
#if UNITY_IOS
        //if we don't need ChargeRecord in iOS IAP system ,set it to false;
        //if SetChargeRecordEnabled is enabled, all purchase records would be sent to pujia8 & Tapdb IAP Server
        GameworksTrackerMgr.Instance.GetPlugin().SetChargeRecordEnabled(true);
#elif UNITY_ANDROID
        //if we need record purchase info with pujia8 IAP Server, set it to false.
        GameworksTrackerMgr.Instance.GetPlugin().SetChargeRecordEnabled(false);
#endif
    }


    void GetItemPriceTable()
    {
        //Get all price(json string dictionary returns)
        GameworkPaymentMgr.Instance.GetPriceTable((string p) =>
        {
            ClearOutput();
            LogOutput(p);
            if (purchaseConsumableButton.interactable)
            {
                GProductPrice price = GameworkPaymentMgr.Instance.GetLocalPrice(pay_item_0);
                purchaseConsumableButtonText.text = price.CurrencyType + price.LocalizedPrice;
            }
            else if (purchaseConsumableButton.interactable)
            {
                GProductPrice price = GameworkPaymentMgr.Instance.GetLocalPrice(pay_item_1);
                purchaseNonConsumableButtonText.text = price.CurrencyType + price.LocalizedPrice;
            }
            RefreshItems();
        });
    }

    void OnPurchaseConsumableButton_Clicked()
    {
        //buy Consumable pay_item_0 test.
        GameworkPaymentMgr.Instance.BuyProduct(pay_item_0,
            (IPaymentCallbackMsg callbackMsg) =>
            {
                //success
                LogOutput(string.Format("Buy {0} Success!", pay_item_0));
            },
            (IPaymentCallbackMsg callbackMsg) => { LogOutput(string.Format("Buy {0} Fail!", pay_item_0)); });
    }

    void OnPurchaseNonConsumableButton_Clicked()
    {
        //buy NonConsumable pay_item_1 test.
        GameworkPaymentMgr.Instance.BuyProduct(pay_item_1,
            (IPaymentCallbackMsg callbackMsg) =>
            {
                //success
                LogOutput(string.Format("Buy {0} Success!", pay_item_1));
                PlayerPrefs.SetInt(pay_item_1, 1);
                PlayerPrefs.Save();
                RefreshItems();
            },
            (IPaymentCallbackMsg callbackMsg) => {
                LogOutput(string.Format("Buy {0} Fail!", pay_item_1));
            });
    }

    void OnGetPriceButton_Clicked()
    {
        ClearOutput();
        //get price table
        GetItemPriceTable();
    }

    void OnGetRestoreButton_Clicked()
    {
        //restore products
        GameworkPaymentMgr.Instance.RestorePurchases((string[] p) =>
        {
            LogOutput(string.Format("Got RestorePurchase ID list {0}", string.Join(",", p)));
            foreach (var value in p)
            {
                if (value.Equals(pay_item_1))
                {
                    PlayerPrefs.SetInt(pay_item_1, 1);
                }
            }

            PlayerPrefs.Save();
            RefreshItems();
        });
    }

    void RefreshItems()
    {
        //try refresh UI
        int pay_item_1_val = PlayerPrefs.GetInt(pay_item_1, 0);
        if (pay_item_1_val == 1)
        {
            purchaseNonConsumableButton.interactable = false;
            purchaseNonConsumableButtonText.text = "Purchased";
        }
        else
        {
            purchaseNonConsumableButton.interactable = true;
            GProductPrice price = GameworkPaymentMgr.Instance.GetLocalPrice(pay_item_1);
            purchaseNonConsumableButtonText.text = price.CurrencyType + price.LocalizedPrice;
        }
    }

    void RemoveAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();

        ClearOutput();
        RefreshItems();
    }

    void ClearOutput()
    {
        outputMsgWindow.text = "";
    }

    void LogOutput(string s)
    {
        outputMsgWindow.text += "\n";
        outputMsgWindow.text += s;
    }
}