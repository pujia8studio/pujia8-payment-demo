package com.moogle.unitybridge;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.util.Log;

import com.moogle.gameworks_payment_java.ChannelStoreManager;
import com.moogle.gameworks_payment_java.Elysium;
import com.moogle.gameworks_payment_java.FrameworkConsts;
import com.moogle.gameworks_payment_java.IPaymentCallback;
import com.moogle.gameworks_payment_java.IPaymentRequest;

import com.moogle.gameworks_payment_java.payment.INetworkEventCallback;
import com.moogle.gameworks_payment_java.utility.Utility;
import com.unity3d.player.UnityPlayer;

import java.util.Map;

public class UnityBridge extends Fragment implements IPaymentCallback, IPaymentRequest {
    private static UnityBridge Instance;
    private Activity mActivity;
    private ChannelStoreManager mChannelStoreManager;

    public static UnityBridge GetInstance() {
        if (Instance == null) {
            Instance = new UnityBridge();
            UnityPlayer.currentActivity.getFragmentManager().beginTransaction().add(Instance, "UnityBridge").commit();
            Log.d("GameworkPaymentMgr", "init Instance");
            Instance.mChannelStoreManager = ChannelStoreManager.GetInstance();
            Instance.mActivity = UnityPlayer.currentActivity;
            Instance.mChannelStoreManager.Initalize(Instance.mActivity);
            Instance.Initalize(Instance);
            Instance.SetEnvironment();

        }
        return Instance;
    }

    public boolean isLoaded() {
        return mChannelStoreManager.isPaymentEnabled;
    }

    @Override
    public void OnProcessPurchaseCallback(String result) {
        Log.d("GameworkPaymentMgr", "OnProcessPurchaseCallback:" + result);
        UnityPlayer.UnitySendMessage("GameworkPaymentAndroidMgr", "OnProcessPurchaseCallback", result);
    }

    @Override
    public void OnPurchaseFailedCallback(String result) {
        Log.d("GameworkPaymentMgr", "OnPurchaseFailedCallback:" + result);
        UnityPlayer.UnitySendMessage("GameworkPaymentAndroidMgr", "OnPurchaseFailedCallback", result);
    }

    @Override
    public void OnRestoreCallback(String result) {
        UnityPlayer.UnitySendMessage("GameworkPaymentAndroidMgr", "OnRestoreCallback", result);

    }

    @Override
    public void OnRequestPurchaseCallback(String result) {
        UnityPlayer.UnitySendMessage("GameworkPaymentAndroidMgr", "OnRequestPurchaseCallback", result);

    }

    @Override
    public void OnPriceGetCallback(String result) {
        if (result.isEmpty()) {
            result = "{}";
        }
        UnityPlayer.UnitySendMessage("GameworkPaymentAndroidMgr", "OnPriceGetCallback", result);
    }


    @Deprecated
    public void InitPlugin() {
        //没用了，置空
    }

    /**
     * 挂载Fragment到game ctivity
     *
     * @param pGameCallback 回调接口
     */
    @Override
    public void Initalize(IPaymentCallback pGameCallback) {
        Elysium.CurrentActivity = UnityPlayer.currentActivity;
        Elysium.Initalize(UnityPlayer.currentActivity);
        mChannelStoreManager.GetPaymentController().Initalize(pGameCallback);
    }

    /**
     * 设定内购系统环境
     */
    @Override
    public void SetEnvironment() {
        mChannelStoreManager.GetPaymentController().SetEnvironment();
    }

    /**
     * 购买道具
     * order是以,分割的字符串,一共四个参数
     * 输入数据 "pay_item_01,商品名,商品描述,consumable"
     */
    public void PurchaseItem(String orderData) {

        if (mChannelStoreManager.isPaymentEnabled) {
            String[] orderInfo = orderData.split(",");
            if (orderInfo.length > 3) {
                String purchaseID = orderInfo[0];
                String itemName = orderInfo[1];
                String itemDesc = orderInfo[2];
                String isConsumable = orderInfo[3];
                if (isConsumable.toLowerCase().equals("true")) {

                    mChannelStoreManager.PurchaseItem(purchaseID, itemName, itemDesc, true);
                } else {
                    mChannelStoreManager.PurchaseItem(purchaseID, itemName, itemDesc, false);
                }
            } else {
                Log.e("UnityBridge", "PurchaseItem() Error , input data error," + orderData);
            }
        } else {
            Log.e("UnityBridge", "PurchaseItem() Error , isPaymentEnabled Not Enabled." + orderData);
        }

    }

    /**
     * "pay_item_01,商品名,商品描述,isconsumable"
     *
     * @param purchaseID   商品ID
     * @param itemName     商品名
     * @param itemDesc     商品描述
     * @param isConsumable 是否可消耗(bool)
     */
    @Override
    public void PurchaseItem(String purchaseID, String itemName, String itemDesc, boolean isConsumable) {
        if (mChannelStoreManager.isPaymentEnabled) {
            mChannelStoreManager.PurchaseItem(purchaseID, itemName, itemDesc, isConsumable);
        } else {
            Log.e("UnityBridge", "PurchaseItem() Error , isPaymentEnabled Not Enabled." + purchaseID);
        }

    }

    /**
     * 不可消费道具会在这里进行网络恢复
     */
    @Override
    public void RestorePurchase() {

        mChannelStoreManager.GetPaymentController().RestorePurchase();
    }

    /**
     * 强制写入不可消费道具和失败道具到内置存储。
     */
    @Override
    public void SaveInventory() {
        mChannelStoreManager.GetPaymentController().SaveInventory();
    }

    /**
     * 获取设备唯一识别码
     */
    @Override
    public String GetFakeDeviceCRC() {
        return mChannelStoreManager.GetPaymentController().GetFakeDeviceCRC();
    }

    /**
     * 启用支付沙箱模式（仅用于测试，正式发布一定要关闭）
     *
     * @param actived
     */
    public void EnablePaymentSandbox(boolean actived) {
        mChannelStoreManager.EnablePaymentSandbox(actived);
    }

    public void GetPriceTable() {
        mChannelStoreManager.GetPaymentController().GetPriceTable();
    }

    public String ReadTextFileFromAssets(String name) {
        return Utility.ReadTextFileFromAssets(mActivity, name);
    }

    public boolean GetNetworkAndIMEI() {
        String uuid = mChannelStoreManager.GetPaymentController().GetFakeDeviceCRC();
        return Utility.isNetworkConnected(mActivity);
    }

    /**
     * 显示Android自带的警告窗口
     *
     * @param title 窗体标题
     * @param desc  窗体描述
     */
    public void ShowAlartDialog(String title, String desc) {
        final String pTitle = title;
        final String pDesc = desc;
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog dialog = new AlertDialog.Builder(mActivity).setTitle(pTitle)
                        .setMessage(pDesc)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }
        });
    }
}
