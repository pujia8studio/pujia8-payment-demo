##Example
Payment-Demo/PurchaseExample.cs Simple example for Pujia8 Studio payment plugin

ChinaMarket/Plugins/android-src/ source code of the payment plugin UnityBridge.

##IAP System
*  Initialization
    *  you can simply call the `GameworkPaymentMgr.Instance.Init(() => { });` method to start the initialization process, ; 
    *  use `GameworkPaymentMgr.Instance.AddProducts()` for adding products.

*   Get Products info
    * you can call the `GameworkPaymentMgr.Instance.GetPriceTable(Action<string> p)` method to get all price(json string dictionary returns).
    * you may call the ` GameworkPaymentMgr.Instance.RestorePurchases(Action<string[]> p)` method to restore all the products you have been purchased. The return string array are the non-consumable product-ids

*   Processing Purchases
    * you can simply buy one product by calling `GameworkPaymentMgr.Instance.BuyProduct(string product_id, Action successCallback, Action failCallback )`.

*   Configuring for Android
    * check the `ChinaMarket/Plugins/Android`. We use android-support-v4 and appcompat-v7 librarys. If you got an error with the support-v4 dependcies. Check theme first.
    * Change your Build system to Gradle.(File - Build Settings -Platform Android -Build System(Gradle) )
    * Change your Package Name in (`File - Build Settings - Player Settings`) -  (`Other Settigns - Identification - PackageName`)
    * Set `Minimum API Level above 4.2 (API Level 17)`.
    * Set `Target API Level above 24`.
    * Set `Device Filter -> ARMV7`.
    * Set your `Publishing keystore` in  `Publishing Settings`.
    * Change Minify Settings `Mninify- Release :None`,`Mninify- Debug :None`.(We use reflections in Java, so don't minify payment aar librarys).
    * Check `Plugins/Android/gameworks_release/AndroidManifest.xml`,make sure the `<meta-data android:name="gameworks_bundle_id" android:value="your.package.name"></meta-data>` is your package name. 

*   Analystic
    * If you need analystic in China by us. Simple init analystic system for TapDb.
    * TapDb service needs an APPID, 
        `See Configs/iOSConfig/TapdbConf.json
        And Plugins/Android/gameworks_release/assets/TapdbConf.json
        Change "TapDB_Tracker_AppID" .`
