﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
    public interface ISingleton
    {
        void OnSingletonInit();
    }
}
