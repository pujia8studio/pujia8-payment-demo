﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

namespace com.moogle.gameworks
{
    public class SingletonCreator
    {
        public static T CreateSingletonMono<T>() where T : MonoBehaviour, ISingleton
        {
            T instance = null;
            instance = GameObject.FindObjectOfType(typeof(T)) as T;
            if (instance == null)
            {
                GameObject obj = new GameObject("[Singleton]" + typeof(T).Name);
                UnityEngine.Object.DontDestroyOnLoad(obj);
                instance = obj.AddComponent<T>();

                instance.OnSingletonInit();
            }
            return instance;
        }

        public static T CreateSingleton<T>() where T : class, ISingleton
        {
            T retInstance = default(T);

            ConstructorInfo[] ctors = typeof(T).GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic);
            ConstructorInfo ctor = Array.Find(ctors, c => c.GetParameters().Length == 0);

            if (ctor == null)
            {
                throw new Exception("Non-public ctor() not found! in " + typeof(T));
            }

            retInstance = ctor.Invoke(null) as T;

            retInstance.OnSingletonInit();

            return retInstance;
        }
    }

}
