﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
    public abstract class Singleton<T> : ISingleton where T : Singleton<T>
    {
        protected static T mInstance = null;
        static object mLock = new object();

        protected Singleton()
        {
        }

        public static T Instance
        {
            get
            {
                lock (mLock)
                {
                    if (mInstance == null)
                    {
                        mInstance = SingletonCreator.CreateSingleton<T>();
                    }
                }

                return mInstance;
            }
        }

        public void Dispose()
        {
            mInstance = null;
        }

        public virtual void OnSingletonInit()
        {
        }
    }

    public abstract class SingletonMono<T> : MonoBehaviour, ISingleton where T : SingletonMono<T>
    {

        protected static T mInstance = null;

        public static T Instance
        {
            get
            {
                if (mInstance == null)
                {
                    mInstance = SingletonCreator.CreateSingletonMono<T>();
                }

                return mInstance;
            }
        }
        public virtual void OnSingletonInit()
        {

        }

        protected virtual void OnDestroy()
        {
            mInstance = null;
        }
    }
}