﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace com.moogle.gameworks
{

    public class FrameworkUtils
    {
        public static string GetRandomUserId()
        {
            string platform = "player";
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    platform = "android";
                    break;
                case RuntimePlatform.IPhonePlayer:
                    platform = "ios";
                    break;

            }
            string uuid = PlayerPrefs.GetString("player-uuid", "");
            if (uuid != "")
            {
                Debug.Log(uuid);
                return uuid;
            }
            else
            {
                // Global Unique IDentifier
                System.Guid guid = System.Guid.NewGuid();
                uuid = guid.ToString().Replace("-", "").ToLower();
                uuid = platform + uuid;
                PlayerPrefs.SetString("player-uuid", uuid);
                PlayerPrefs.Save();
                return uuid;
            }
        }



        public static string JsonSerialize(object inputClass, bool isIntend = false)
        {
            Formatting fmt = Formatting.None;
            if (isIntend)
            {
                fmt = Formatting.Indented;
            }
            string jsondata = JsonConvert.SerializeObject(inputClass, fmt);
            return jsondata;
        }

        internal static T JsonDeserialize<T>(string json) where T : class
        {
            json = json.Trim(new char[] { '\uFEFF' });
            JsonSerializer serializer = new JsonSerializer();
            StringReader sr = new StringReader(json);
            object obj = serializer.Deserialize(new JsonTextReader(sr), typeof(T));
            T t = obj as T;
            return t;
        }
    }



}