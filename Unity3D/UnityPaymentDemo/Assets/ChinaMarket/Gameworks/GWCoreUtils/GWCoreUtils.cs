﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;
using System.Text;

public class GWCoreUtils
{
    public static string GetMD5Hash(string msg)
    {  
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();  
        byte[] data = new UTF8Encoding(false).GetBytes(msg);  
        byte[] md5Data = md5.ComputeHash(data, 0, data.Length);  
        md5.Clear();  

        string destString = "";  
        for (int i = 0; i < md5Data.Length; i++) {  
            destString += System.Convert.ToString(md5Data[i], 16).PadLeft(2, '0');  
        }  
        destString = destString.PadLeft(32, '0');  
        return destString;
    }
    
#if UNITY_ANDROID
    public static string JAVA_CLASS = "com.moogle.gwjniutils.gwcoreutils.GWCoreUtils";
    private static string UNTIY_JAVA_CLASS = "com.unity3d.player.UnityPlayer";
    private static AndroidJavaClass proxy = null;
    private static AndroidJavaClass unityClass = null;

    private static AndroidJavaClass getUtilClass()
    {
        if (proxy == null)
        {
            proxy = new AndroidJavaClass(JAVA_CLASS);
        }

        return proxy;
    }

    private static AndroidJavaClass getUnityClass()
    {
        if (unityClass == null)
        {
            unityClass = new AndroidJavaClass(UNTIY_JAVA_CLASS);
        }

        return unityClass;
    }

    private static AndroidJavaObject getUnityActivity()
    {
        AndroidJavaObject activity = getUnityClass().GetStatic<AndroidJavaObject>("currentActivity");
        return activity;
    }

    public static void SocialShare(string title, string desc, string url, string imagePath)
    {
        string text = "";
        if (!string.IsNullOrEmpty(title))
        {
            text = string.Format("【{0}】\n{1} {2}", title, desc, url);
        }
        else
        {
            text = string.Format("{0} {1}", desc, url);
        }
        
        AndroidJavaObject activity = getUnityActivity();
        getUtilClass().CallStatic("openShareDialog", activity, text, imagePath);
        
    }

    public static string ReadTextFileFromAssets(string fileName)
    {
        try
        {
            AndroidJavaObject activity = getUnityActivity();
            return getUtilClass().CallStatic<string>("readTextFileFromAssets", activity, fileName);
        }
        catch
        {
        }

        return string.Empty;
    }

    public static string EncodeBase64(string value)
    {
        try
        {
            return getUtilClass().CallStatic<string>("encodeBase64", value);
        }
        catch
        {
        }

        return string.Empty;
    }

    public static string DecodeBase64(string value)
    {
        try
        {
            return getUtilClass().CallStatic<string>("decodeBase64", value);
        }
        catch
        {
        }

        return string.Empty;
    }

    public static string GetDeviceCRC()
    {
        try
        {
            return getUtilClass().CallStatic<string>("getDeviceCRC");
        }
        catch
        {
        }

        return "unknown";
    }

    public static string GetAppName()
    {
        try
        {
            AndroidJavaObject activity = getUnityActivity();
            return getUtilClass().CallStatic<string>("getAppName", activity);
        }
        catch
        {
        }

        return "unknown";
    }

    public static string GetAppVersion()
    {
        try
        {
            AndroidJavaObject activity = getUnityActivity();
            return getUtilClass().CallStatic<string>("getAppVersion", activity);
        }
        catch
        {
        }

        return "unknown";
    }

    public static bool IsDebugVersion()
    {
        try
        {
            AndroidJavaObject activity = getUnityActivity();
            return getUtilClass().CallStatic<bool>("isDebugVersion", activity);
        }
        catch
        {
        }

        return false;
    }

    public static void ShowAlertDialog(string title, string desc)
    {
        if (Application.isEditor)
        {
            Debug.LogWarningFormat("[{0}]{1}", title, desc);
            return;
        }
        try
        {
            AndroidJavaObject activity = getUnityActivity();
            getUtilClass().CallStatic("showAlertDialog", activity, title, desc);
        }
        catch
        {
        }
    }

    public static void SafeCheck(string md5Hash)
    {
        md5Hash = md5Hash.Replace(" ", "");
        string p = GetSignatureMD5Hash();
        if (p.Equals(md5Hash))
        {
        }
        else
        {
            //Debug.LogError("Lol :" + Encrypter.Base64Encode(p));
            Application.Quit();
        }
    }
    
    public static string GetSignatureMD5Hash()
    {
        var player = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var activity = player.GetStatic<AndroidJavaObject>("currentActivity");
        var PackageManager = new AndroidJavaClass("android.content.pm.PackageManager");
        
        var packageName = activity.Call<string>("getPackageName");
        
        var GET_SIGNATURES = PackageManager.GetStatic<int>("GET_SIGNATURES");
        var packageManager = activity.Call<AndroidJavaObject>("getPackageManager");
        var packageInfo = packageManager.Call<AndroidJavaObject>("getPackageInfo", packageName, GET_SIGNATURES);
        var signatures = packageInfo.Get<AndroidJavaObject[]>("signatures");
        if(signatures != null && signatures.Length > 0)
        {
            byte[] bytes = signatures[0].Call<byte[]>("toByteArray");
            
            return GetMD5Hash(bytes);
        }

        return string.Empty;
    }
    
    private static string GetMD5Hash(byte[] data) {
        try {
            data = DecryptXor(data);
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(data);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < retVal.Length; i++) {
                sb.Append(retVal[i].ToString("x2"));
            }
            return sb.ToString();
        } catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return string.Empty;
        }
    }

#endif
    
    class Encrypter
    {
        /// <summary>
        /// Base64加密，采用utf8编码方式加密
        /// </summary>
        /// <param name="source">待加密的明文</param>
        /// <returns>加密后的字符串</returns>
        public static string Base64Encode(string source)
        {
            return Base64Encode(Encoding.UTF8, source);
        }

        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="encodeType">加密采用的编码方式</param>
        /// <param name="source">待加密的明文</param>
        /// <returns></returns>
        public static string Base64Encode(Encoding encodeType, string source)
        {
            string encode = string.Empty;
            byte[] bytes = encodeType.GetBytes(source);
            try
            {
                encode = Convert.ToBase64String(bytes);
            }
            catch
            {
                encode = source;
            }
            return encode;
        }

        /// <summary>
        /// Base64解密，采用utf8编码方式解密
        /// </summary>
        /// <param name="result">待解密的密文</param>
        /// <returns>解密后的字符串</returns>
        public static string Base64Decode(string result)
        {
            return Base64Decode(Encoding.UTF8, result);
        }

        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="encodeType">解密采用的编码方式，注意和加密时采用的方式一致</param>
        /// <param name="result">待解密的密文</param>
        /// <returns>解密后的字符串</returns>
        public static string Base64Decode(Encoding encodeType, string result)
        {
            string decode = string.Empty;
            byte[] bytes = Convert.FromBase64String(result);
            try
            {
                decode = encodeType.GetString(bytes);
            }
            catch
            {
                decode = result;
            }
            return decode;
        }
        
        public static void Decode(byte[] src, byte[] key)
        {
            Encode(src, key);
        }
        public static void Encode(byte[] src, byte[] key)
        {
            for (int i = 0; i < src.Length; i++)
            {
                src[i] = (byte)(src[i] ^ key[i % key.Length]);
            }
        }
    }
    
    private static byte[] DecryptXor(byte[] src)
    {
        //init xor key
        int[] numArray = new int[] { -1387743643, 0x132f087a, -380916995, 0x4271fbc6,
            0x481e49b7, 0x839d7f1, -893766998, -1242421477,
            -1230126924, 0x255d7a9e, 0x64659018 };
        byte[] key = new byte[numArray.Length * 4];
        for (int i = 0; i < key.Length; i++)
        {
            key[i] = (byte)((numArray[i / 4] >> ((i % 4) * 8)) & 0xff);
        }
        Encrypter.Decode(src, key);
        return src;
    }
}