﻿#if UNITY_IOS
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

namespace com.moogle.gameworks
{

    public class PaymentIOSListener : MonoBehaviour, IStoreListener
    {
        private int retryWatchDog = 4;
        private string errorReason = "";
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            GameworkPaymentIOSMgr.Instance.OnInitialized(controller, extensions);
            //Debug.Log("Purchaser OnInitialized");
            errorReason = "";
        }

        public string GetErrorReason()
        {
            return errorReason;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            errorReason = "";
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
            if (error == InitializationFailureReason.AppNotKnown)
            {
                Debug.Log("PaymentIOSListener AppNotKnown");
                errorReason = "App Not Known";
            }
            else if (error == InitializationFailureReason.NoProductsAvailable)
            {
                Debug.Log("PaymentIOSListener NoProductsAvailable");
                errorReason = "No Products Available";
            }
            else if (error == InitializationFailureReason.PurchasingUnavailable)
            {
                Debug.Log("PaymentIOSListener PurchasingUnavailable");
                errorReason = "Purchasing Unavailable ";
            }
            else
            {
                
            }
            
            if (retryWatchDog > 0)
            {
                retryWatchDog--;
                GameworkPaymentIOSMgr.Instance.SetEnvironment();
            }
            else
            {
                retryWatchDog = 0;
            }
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",
                product.definition.storeSpecificId, failureReason));
            GameworkPaymentIOSMgr.Instance.OnPurchaseFailedCallback(product.definition.id);
        }
        
        
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            if (GameworkPaymentIOSMgr.Instance.RestoreFlag == 1)
            {
                Debug.Log("Restore ios purchase items");
                var itemID = e.purchasedProduct.definition.id;
                if (GameworkPaymentIOSMgr.Instance.GetProductDictionary().ContainsKey(itemID))
                {
                    if (GameworkPaymentIOSMgr.Instance.GetProductDictionary()[itemID].Consumable == false)
                    {
                        GameworkPaymentIOSMgr.Instance.RestoreList.Add(itemID);
                    }
                    
                }
                if (GameworkPaymentIOSMgr.Instance.RestoreCompleteFlag == 1)
                {
                    return PurchaseProcessingResult.Complete;
                }
                return PurchaseProcessingResult.Complete;
            }

            Debug.Log("Purchaser ProcessPurchase...");
            string kReceiptcrc = "000000";
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.tvOS) {
                string transactionReceipt = GameworkPaymentIOSMgr.StoreExtensionProvider.GetExtension<IAppleExtensions>().GetTransactionReceiptForProduct (e.purchasedProduct);
                kReceiptcrc = GWCoreUtils.GetMD5Hash(transactionReceipt);
                // Send transaction receipt to server for validation
            }

            //Debug.Log("id=" + e.purchasedProduct.definition.id);
            //Debug.Log("productId=" + GameworkPaymentIOSMgr.Instance.CurrentPurchaseId);
            var productId = GameworkPaymentIOSMgr.Instance.CurrentPurchaseId;
            

            if (String.Equals(e.purchasedProduct.definition.id, productId, StringComparison.Ordinal)
                && LocalHashTransaction(productId, e.purchasedProduct.receipt))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", kReceiptcrc));
                StringBuilder sb = new StringBuilder();
                sb.Append(productId);
                sb.Append(",");
                sb.Append(GamepayConst.RESULT_CODE_SUCCESS);
                sb.Append(",");
                sb.Append(kReceiptcrc);
                sb.Append(",");
                GameworkPaymentIOSMgr.Instance.OnProcessPurchaseCallback(sb.ToString());
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(productId);
                sb.Append(",");
                sb.Append(GamepayConst.RESULT_CODE_FAIL);
                sb.Append(",");
                sb.Append(kReceiptcrc);
                sb.Append(",");
                GameworkPaymentIOSMgr.Instance.OnProcessPurchaseCallback(sb.ToString());
            }

            return PurchaseProcessingResult.Complete;
        }


        private bool LocalHashTransaction(string productID, string unityIAPReceipt)
        {
            bool validPurchase = true;
#if UNITY_IOS && !UNITY_EDITOR
            string appleBundleId = Application.identifier;
            var apple = new AppleValidator(AppleCertTangle.Data());

            try
            {
                Dictionary<string, object> dictionary1 =
                    (Dictionary<string, object>) MiniJson.JsonDecode(unityIAPReceipt);
                string str1 = (string) dictionary1["Store"];
                string str2 = (string) dictionary1["Payload"];
                AppleReceipt appleReceipt = apple.Validate(Convert.FromBase64String(str2));
                validPurchase = false;
                if (appleReceipt == null)
                {
                    validPurchase = false;
                }
                else if (appleBundleId.Equals(appleReceipt.bundleID))
                {
                    IPurchaseReceipt[] receiptData =
                        (IPurchaseReceipt[]) ((IEnumerable<AppleInAppPurchaseReceipt>) appleReceipt
                            .inAppPurchaseReceipts).ToArray<AppleInAppPurchaseReceipt>();
                    foreach (IPurchaseReceipt productReceipt in receiptData)
                    {
                        Debug.Log(productReceipt.productID);
                        Debug.Log(productReceipt.purchaseDate);
                        Debug.Log(productReceipt.transactionID);
                        if (string.Equals(productReceipt.productID, productID, StringComparison.InvariantCultureIgnoreCase))
                        {
                            validPurchase = true;
                            break;
                        }
                    }
                }
                else
                {
                    validPurchase = false;
                }
            }
            catch (Exception e)
            {
                Debug.Log("Invalid receipt, not unlocking content");
                validPurchase = false;
            }
#endif
            return validPurchase;
        }
    }
}
#endif