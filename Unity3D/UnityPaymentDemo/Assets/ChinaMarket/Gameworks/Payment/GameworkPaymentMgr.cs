﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace com.moogle.gameworks
{


    public class GameworkPaymentMgr : SingletonMono<GameworkPaymentMgr>, IPaymentMgr
    {
        IPaymentMgr platformPayMgr;
        bool isInit = false;
        public void Init(Action onInitAction)
        {
            if (isInit)
            {
                return;
            }

            isInit = true;
#if UNITY_ANDROID && !UNITY_EDITOR
            GameworkPaymentAndroidMgr.Instance.Init(onInitAction);
            platformPayMgr = GameworkPaymentAndroidMgr.Instance;
            return;
#elif UNITY_EDITOR
            GameworkPaymentPCStubMgr.Instance.Init(onInitAction);
            platformPayMgr = GameworkPaymentPCStubMgr.Instance;

#elif UNITY_IOS
            GameworkPaymentIOSMgr.Instance.Init(onInitAction);
            platformPayMgr = GameworkPaymentIOSMgr.Instance;
            return;
#endif
        }

        public void AddProducts(GameStoreProduct product)
        {
            if (platformPayMgr != null)
                platformPayMgr.AddProducts(product);
            else
            {
                if (!GameworkPaymentMgr.Instance.ProductDictionary.ContainsKey(product.ProductID))
                {
                    Debug.Log("[GameworksPaymentAndroidMgr]add product " + product.ProductID);
                    GameworkPaymentMgr.Instance.ProductDictionary.Add(product.ProductID, product);
                }
            }
        }

        public void SetEnvironment()
        {
            platformPayMgr.SetEnvironment();
        }

        public void GetPriceTable(Action<string> onPriceTableGetcallback)
        {
            platformPayMgr.GetPriceTable(onPriceTableGetcallback);
        }

        /// <summary>
        /// The easiest way. Buy Product with simple productID (purchaseid).
        /// </summary>
        /// <param name="productID">purchaseid</param>
        /// <param name="successCallback">while purchase success, invoke system.action</param>
        /// <param name="failCallback">while purchase fails , invoke system.action</param>
        public void BuyProduct(string productID, Action<IPaymentCallbackMsg> successCallback, Action<IPaymentCallbackMsg> failCallback)
        {
            if (!GameworkPaymentMgr.Instance.GetProductDictionary().ContainsKey(productID))
            {
                Debug.LogErrorFormat("[GameworksPaymentMgr] product_id: [{0}] not exist, please try AddProducts()", productID);
                return;
            }
            var tproduct = GameworkPaymentMgr.Instance.GetProductDictionary()[productID];
            GameStorePurchaserOptions opt;
            if (tproduct.Consumable)
            {
                opt = new GameStorePurchaserOptions()
                {
                    product = productID,
                    consumableDelegate = successCallback,
                    nonconsumableDelegate = null,
                    failDelegate = failCallback
                };
            }
            else
            {
                opt = new GameStorePurchaserOptions()
                {
                    product = productID,
                    consumableDelegate = null,
                    nonconsumableDelegate = successCallback,
                    failDelegate = failCallback
                };
            }
            this.BuyProduct(opt);
        }


        public void BuyProduct(GameStorePurchaserOptions option)
        {
            StartCoroutine(CoroBuyItem(option, 0.5f));

        }

        private IEnumerator CoroBuyItem(GameStorePurchaserOptions option, float time)
        {
            yield return new WaitForSeconds(time);
            if (platformPayMgr != null)
                platformPayMgr.BuyConsumable(option);
        }

        [Obsolete]
        public void BuyConsumable(GameStorePurchaserOptions option)
        {

            if (platformPayMgr != null)
                platformPayMgr.BuyConsumable(option);
        }

        public Dictionary<string, GameStoreProduct> GetProductDictionary()
        {
            if (platformPayMgr != null)
                return platformPayMgr.GetProductDictionary();
            else return new Dictionary<string, GameStoreProduct>();
        }

        public void RestorePurchases(Action<string[]> p)
        {
            if (platformPayMgr != null)
                platformPayMgr.RestorePurchases(p);
        }

        public void EnablePaymentSandbox(bool value)
        {
            if (platformPayMgr != null)
                platformPayMgr.EnablePaymentSandbox(value);
        }

        internal static Dictionary<string, GProductPrice> PriceDict = new Dictionary<string, GProductPrice>() {
            //商品ID， 参考价格(人民币)，
            /*{ "item_zh_1", 6 } ,
            { "item_zh_2", 2 },
            { "item_zh_3", 1 }*/
        };
        
        

        public GProductPrice GetLocalPrice(string productId)
        {
            if (PriceDict.ContainsKey(productId))
            {
                return PriceDict[productId];
            }
            return GProductPrice.GetDefaultPrice();
        }


        public static string GetCurrencyTag(string CurrencyType)
        {
            if (CurrencyType.ToUpper().Equals("CNY"))
            {
                return "￥";
            }
            else if (CurrencyType.ToUpper().Equals("JPY"))
            {
                return "￥";
            }
            else if (CurrencyType.ToUpper().Equals("EUR"))
            {
                return "€";
            }
            else if (CurrencyType.ToUpper().Equals("EUR"))
            {
                return "€";
            }
            else
            {
                return "$";
            }
        }


        internal Dictionary<string, GameStoreProduct> ProductDictionary = new Dictionary<string, GameStoreProduct>();
    }

}
