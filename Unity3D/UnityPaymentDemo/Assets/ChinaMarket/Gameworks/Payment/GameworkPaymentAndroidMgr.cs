﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using com.moogle.gameworks;

namespace com.moogle.gameworks
{


    public enum E_GameStoreConsumable
    {
        Consumable = 0,
        NonConsumable
    }

    public class GameStoreProduct
    {
        public string ProductID;
        public string ProductName;
        public string ProductDesc;
        public bool Consumable;

        public GameStoreProduct(string pProductID, E_GameStoreConsumable pConsumable)
        {
            ProductID = pProductID;
            ProductName = pProductID;
            ProductDesc = pProductID;
            if (pConsumable == E_GameStoreConsumable.Consumable) Consumable = true;
            if (pConsumable == E_GameStoreConsumable.NonConsumable) Consumable = false;
        }

        public GameStoreProduct(string pProductID, string pProductName, string pProductDesc, E_GameStoreConsumable pConsumable)
        {
            ProductID = pProductID;
            ProductName = pProductName;
            ProductDesc = pProductDesc;
            if (pConsumable == E_GameStoreConsumable.Consumable) Consumable = true;
            if (pConsumable == E_GameStoreConsumable.NonConsumable) Consumable = false;
        }
    }

    public class GameStorePurchaserOptions
    {
        public string product;
        public System.Action<IPaymentCallbackMsg> consumableDelegate;
        public System.Action<IPaymentCallbackMsg> nonconsumableDelegate;
        public System.Action<IPaymentCallbackMsg> failDelegate;
    }


#if UNITY_ANDROID
    [DisallowMultipleComponent]
    public class GameworkPaymentAndroidMgr : MonoBehaviour, IStoreCallback, IPaymentMgr
    {

        private static GameworkPaymentAndroidMgr s_instance;
        public static GameworkPaymentAndroidMgr Instance
        {
            get
            {
                if (s_instance == null)
                {
                    return new GameObject("GameworkPaymentAndroidMgr").AddComponent<GameworkPaymentAndroidMgr>();
                }
                else
                {
                    return s_instance;
                }
            }
        }
        private bool m_bInitialized;
        public static bool Initialized
        {
            get
            {
                return Instance.m_bInitialized;
            }
        }
        private void Awake()
        {
            if (s_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            s_instance = this;
            DontDestroyOnLoad(gameObject);
            m_bInitialized = true;
        }

        private void OnEnable()
        {
            if (s_instance == null)
            {
                s_instance = this;
            }

            if (!m_bInitialized)
            {
                return;
            }
        }

        private void OnDestroy()
        {
            if (s_instance != this)
            {
                return;
            }

            s_instance = null;

            if (!m_bInitialized)
            {
                return;
            }
        }

        private void Update()
        {
            if (!m_bInitialized)
            {
                return;
            }
        }
        private bool isPurchasing = false;
        private Action onInitAction;
        public void Init(Action onInitCallback)
        {
            if (m_bInitialized){
                return;
            }
            m_bInitialized = true;
            this.mBridgeInstance = new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            this.mBridgeInstance.Call("InitPlugin");
            //string jsondata = Internal_ReadFromAssets("gamepayConf.json");
            onInitAction = onInitCallback;
        }

        public void SetEnvironment()
        {
            Debug.Log("mGameworksInstance.Call(SetEnvironment);");
            if (this.mBridgeInstance == null)
            {
                this.mBridgeInstance =
                    new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            }
            this.mBridgeInstance.Call("SetEnvironment");
            Debug.Log("onInitAction.Invoke();");
            if (onInitAction != null)
                onInitAction.Invoke();
        }


        public Dictionary<string, GameStoreProduct> GetProductDictionary()
        {
            return GameworkPaymentMgr.Instance.ProductDictionary;
        }

        

        public void AddProducts(GameStoreProduct product)
        {
            if (!GameworkPaymentMgr.Instance .ProductDictionary.ContainsKey(product.ProductID))
            {
                Debug.Log("[GameworksPaymentAndroidMgr]add product " + product.ProductID);
                GameworkPaymentMgr.Instance.ProductDictionary.Add(product.ProductID, product);
            }

        }

        public void BuyConsumable(GameStorePurchaserOptions options)
        {
            consumableDelegate = options.consumableDelegate;
            nonconsumableDelegate = options.nonconsumableDelegate;
            failDelegate = options.failDelegate;
            BuyProduct(options.product);
        }

        public void GetPriceTable(Action<string> onPriceTableGetcallback)
        {
            onPriceTableGet = onPriceTableGetcallback;
            Internal_GetPriceTable();
        }



        private void BuyProduct(string curProductID)
        {
            if (isPurchasing == true) return;
            isPurchasing = true;
            if (Application.platform != RuntimePlatform.Android)
            {
                if (GetProductDictionary().ContainsKey(curProductID))
                {
                    Debug.Log("product found :" + curProductID);
                    GameStoreProduct product = GetProductDictionary()[curProductID];
                    if (product.Consumable)
                    {
                        if (consumableDelegate != null)
                            consumableDelegate.Invoke(new PaymentCallbackMsg(curProductID));
                    }
                    else
                    {
                        if (nonconsumableDelegate != null)
                            nonconsumableDelegate.Invoke(new PaymentCallbackMsg(curProductID));
                    }
                }
                else
                {
                    Debug.LogError("not product found :" + curProductID);
                    if (failDelegate != null)
                        failDelegate.Invoke(new PaymentCallbackMsg("Item not avaliable."));
                }
                consumableDelegate = null;
                nonconsumableDelegate = null;
                failDelegate = null;
                isPurchasing = false;
                return;
            }

            if (!Internal_GetNetworkAndIMEI())
            {
                ShowSystemAlert("错误", "当前网络不可用。\n请确认已开启网络或已获取网络权限");
                if (failDelegate != null)
                    failDelegate.Invoke(new PaymentCallbackMsg("Network Error"));
                consumableDelegate = null;
                nonconsumableDelegate = null;
                failDelegate = null;
                return;
            }
            if (GetProductDictionary().ContainsKey(curProductID))
            {
                GameStoreProduct product = GetProductDictionary()[curProductID];
                if (product != null)
                {
                    Debug.Log("product=" + product);
                    Debug.Log("product.isConsumable=" + product.Consumable);
                    StringBuilder textBuilder = new StringBuilder();
                    textBuilder.Append(product.ProductID);
                    textBuilder.Append(",");
                    textBuilder.Append(product.ProductName);
                    textBuilder.Append(",");
                    textBuilder.Append(product.ProductDesc);
                    textBuilder.Append(",");
                    if (product.Consumable)
                    {
                        textBuilder.Append("true");
                    }
                    else
                    {
                        textBuilder.Append("false");
                    }
                    textBuilder.Append(",");
                    Internal_PurchaseItem(textBuilder.ToString());
                    return;
                }
            }
            else
            {
                ShowSystemAlert("错误", "暂时无法购买指定的商品");
                if (failDelegate != null)
                    failDelegate.Invoke(new PaymentCallbackMsg("Item not avaliable."));
                consumableDelegate = null;
                nonconsumableDelegate = null;
                failDelegate = null;
                return;
            }
            consumableDelegate = null;
            nonconsumableDelegate = null;
            failDelegate = null;
            return;

        }

        public void RestorePurchases(Action<string[]> p)
        {
            restoreDelegate = null;
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                Debug.LogWarning("RestorePurchases ");
                restoreDelegate = p;
                Internal_RestorePurchase();
            }
        }

        public void SavePaymentState()
        {
            Internal_SavePayState();
        }

        /**
         * 购买道具
         * order是以,分割的字符串,一共四个参数
         * "pay_item_01,商品名,商品描述,consumable"
         * */
        internal void Internal_PurchaseItem(string order)
        {
            AndroidJavaObject mGameworksInstance;
            mGameworksInstance = new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            mGameworksInstance.Call("PurchaseItem", order);
        }

        internal void Internal_RestorePurchase()
        {
            AndroidJavaObject mGameworksInstance;
            mGameworksInstance = new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            mGameworksInstance.Call("RestorePurchase");
        }

        internal bool Internal_GetNetworkAndIMEI()
        {
            AndroidJavaObject mGameworksInstance;
            mGameworksInstance = new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            return mGameworksInstance.Call<bool>("GetNetworkAndIMEI");
        }

        internal string Internal_ReadFromAssets(string configName)
        {
            AndroidJavaObject mGameworksInstance;
            mGameworksInstance = new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            Debug.Log("[PAYMENTMGR] Internal_ReadFromAssets:" + configName);
            string result = mGameworksInstance.Call<string>("ReadTextFileFromAssets", configName);
            Debug.Log(result);
            return result;
        }

        internal void Internal_SavePayState()
        {
            AndroidJavaObject mGameworksInstance;
            mGameworksInstance = new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            mGameworksInstance.Call("SaveInventory");
        }

        internal void Internal_GetPriceTable()
        {
            AndroidJavaObject mGameworksInstance;
            mGameworksInstance = new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            mGameworksInstance.Call("GetPriceTable");
        }



        public void OnProcessPurchaseCallback(string callbackStr)
        {
            isPurchasing = false;
            Debug.LogWarning("OnProcessPurchaseCallback" + callbackStr);
            //回调购买数据
            //数据格式 productid,result_status
            if (callbackStr.Contains(","))
            {
                string[] param = callbackStr.Split(","[0]);
                string temp_productid = param[0];
                string temp_status = param[1];
                string temp_tradeno = param[2];
                if (temp_status.Equals(GamepayConst.RESULT_CODE_SUCCESS)  && GetProductDictionary().ContainsKey(temp_productid))
                {
                    GameStoreProduct tempProduct = GetProductDictionary()[temp_productid];
                    if (GameworkPaymentMgr.PriceDict.ContainsKey(temp_productid))
                    {
                        this.OnRecordPurchaseSuccsee(temp_tradeno, temp_productid);
                    }
                    if (tempProduct.Consumable)
                    {
                        Debug.LogWarning("consumableDelegate" + callbackStr);
                        if (consumableDelegate != null)
                        {
                            
                            consumableDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                            consumableDelegate = null;
                            nonconsumableDelegate = null;
                            failDelegate = null;
                            return;
                        }
                    }
                    else
                    {
                        Debug.LogWarning("nonconsumableDelegate" + callbackStr);
                        if (nonconsumableDelegate != null)
                        {
                            nonconsumableDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                            consumableDelegate = null;
                            nonconsumableDelegate = null;
                            failDelegate = null;
                            return;
                        }
                    }
                }
                else
                {
                    if (failDelegate != null)
                        failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                    return;
                }
            }
            else
            {
                if (failDelegate != null)
                    failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                return;
            }
            if (failDelegate != null)
                failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));


        }

        public void OnPurchaseFailedCallback(string callbackStr)
        {
            isPurchasing = false;
            Debug.LogWarning("OnPurchaseFailedCallback");
            if (failDelegate != null)
                failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));

        }

        public void OnRestoreCallback(string callbackStr)
        {
            List<string> klist = new List<string>();
            Dictionary<string, string> vdict = new Dictionary<string, string>();
            if (callbackStr.Contains("{") && callbackStr.Contains("}"))
            {
                try
                {
                    vdict = FrameworkUtils.JsonDeserialize<Dictionary<string, string>>(callbackStr);
                }
                catch (Exception e)
                {

                }
            }
            foreach (var s in vdict.Keys)
            {
                if (vdict[s].Equals("true"))
                {
                    klist.Add(s);
                }
            }
            string[] temp_productids = klist.ToArray();
            if (restoreDelegate != null)
            {
                Debug.LogWarning("OnRestoreCallback:" + callbackStr);
                restoreDelegate.Invoke(temp_productids);
                SavePaymentState();
                return;
            }
            Debug.LogWarning("OnRestoreCallback delegate is null");
            restoreDelegate = null;

        }

        public void OnRequestPurchaseCallback(string callback)
        {
            Debug.LogWarning("OnRequestPurchaseCallback :" + callback);
            String tradeNo = "";
            String purchaseID = "";
            String amount = "";
            String payment = "";
            if (callback.Contains(","))
            {
                String[] valuedata = callback.Split(',');
                if (valuedata.Length >= 4)
                {
                    tradeNo = valuedata[0];
                    purchaseID = valuedata[1];
                    amount = valuedata[2];
                    payment = valuedata[3];
                    GProductPrice price = GameworkPaymentMgr.Instance.GetLocalPrice(purchaseID);
                    OnRecordPurchaseRequest(tradeNo, purchaseID, price.Price, price.CurrencyType, payment);
                }
            }
        }


        public void OnPriceGetCallback(string callback)
        {
            Debug.LogWarning("OnPriceGetCallback :" + callback);
            if (callback.Contains("{") && callback.Contains("}"))
            {
                try
                {
                    GameworkPaymentMgr.PriceDict = FrameworkUtils.JsonDeserialize<Dictionary<string, GProductPrice>>(callback);
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message.ToString());
                }
            }
            if (onPriceTableGet != null) onPriceTableGet.Invoke(callback);
        }


        Action<string> onPriceTableGet;
        Action<IPaymentCallbackMsg> consumableDelegate;
        Action<IPaymentCallbackMsg> nonconsumableDelegate;
        Action<IPaymentCallbackMsg> failDelegate;
        Action<string[]> restoreDelegate;


        /// <summary>
        /// 显示系统弹窗
        /// </summary>
        /// <param name="title"></param>
        /// <param name="desc"></param>
        public void ShowSystemAlert(string title, string desc)
        {
            Debug.LogWarning(title + ":" + desc);
            if (this.mBridgeInstance == null)
            {
                this.mBridgeInstance =
                    new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            }
            this.mBridgeInstance.Call("ShowAlartDialog", title, desc);


        }

        /// <summary>
        /// Enable sandbox mode for Alipay 
        /// </summary>
        /// <param name="actived"></param>

        public void EnablePaymentSandbox(bool actived)
        {
            Debug.LogFormat("Sandbox enabled :{0}", actived);
            if (this.mBridgeInstance == null)
            {
                this.mBridgeInstance =
                    new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            }
            this.mBridgeInstance.Call("EnablePaymentSandbox", actived);
        }


        public class AndroidConfig
        {
            public static string MGR_CLASS_NAME = "com.moogle.unitybridge.UnityBridge";
            public QAndroidConfigFile Config;

            public void ReadConfig(string json)
            {
                QAndroidConfigFile curConfig = AndroidConfig.Load(json);
                Config = curConfig;
            }

            public static QAndroidConfigFile Load(string json)
            {
                json = json.Trim(new char[] { '\uFEFF' });
                if (json.Contains("{") && json.Contains("}"))
                {
                    Debug.Log("Load QAndroidConfigFile... ok");
                    return FrameworkUtils.JsonDeserialize<QAndroidConfigFile>(json);
                }
                Debug.Log("Load QAndroidConfigFile... failed");
                return new QAndroidConfigFile();
            }

            [Serializable]
            public class QAndroidConfigFile
            {
                public string WXPAY_APP_ID;
                public string ALIPAY_APP_ID;
                public string SUB_CHANNEL_ID;
                public string PRUCHASE_BASE_UR;
                public bool ACTIVE_ALIPAY = true;
                public bool ACTIVE_WXPAY = true;
                public bool USE_DEBUG_MODE = false;
            }
        }

        public void OnRecordPurchaseRequest(string tradeNo, string purchaseid, string price_yuan, string currencyType, string paymentMethod)
        {
            float float_price_yuan = 0f;
            float.TryParse(price_yuan, out float_price_yuan);

            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseRequest(tradeNo, purchaseid, float_price_yuan, currencyType, paymentMethod);
        }

        public void OnRecordPurchaseSuccsee(string orderId, string productId)
        {
            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseSuccess(orderId, productId);
        }

        public void OnRecordPurchaseFail(string orderId, string productId)
        {
            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseFail(orderId, productId);
            
        }

        public void OnRecordPurchaseSuccessOnly(string tradeNo, string productId, string price_yuan,
            string currencyType, string tradeType)
        {
            float float_price_yuan = 0f;
            float.TryParse(price_yuan, out float_price_yuan);
            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseSuccessOnly(tradeNo, productId, float_price_yuan,
                currencyType, tradeType);
        }

        private AndroidJavaObject mBridgeInstance;

        public AndroidJavaObject getAndroidInstance()
        {
            if (this.mBridgeInstance == null)
            {
                this.mBridgeInstance =
                    new AndroidJavaObject(AndroidConfig.MGR_CLASS_NAME).CallStatic<AndroidJavaObject>("GetInstance");
            }
            return this.mBridgeInstance;
        }
    }

#endif
    public class GamepayConst
    {
        public const int REQUEST_TYPE_ALIPAY = 0;
        public const int REQUEST_TYPE_WXPAY = 1;
        public const string RESULT_CODE_SUCCESS = "9000";
        public const string RESULT_CODE_PAYING = "8000";
        public const string RESULT_CODE_FAIL = "4000";
        public const string RESULT_CODE_CANCEL = "6001";
        public const string RESULT_CODE_NETWORK_ERR = "6002";
        public const string RESULT_CODE_UNKNOWN = "6004";
        public const string RESULT_CODE_ALLUNKNOWN = "0000";
        public const string UNKNOWN = "unknown";
        public const string REQUEST_STATE_PURCHASE = "dopurchase";
        public const string REQUEST_STATE_RESTORE = "restorepurchase";
        public const string REQUEST_PAYMETHOD_ALIPAY = "alipay";
        public const string REQUEST_PAYMETHOD_WXPAY = "wxpay";
    }


}



