﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using com.moogle.gameworks;

public class FakeStoreDialog : MonoBehaviour {

    const string mDefaultMsg = @"Buy {@@purchase_id} ?";
    Text mDialogTitle;
    Text mDialogMessage;

    Button mButtonOK;
    Button mButtonCancel;
    Action<IPaymentCallbackMsg> t_successCallback;
    Action<IPaymentCallbackMsg> t_failCallback;
    

    void InitComponents()
    {
        mDialogTitle = transform.Find("DialogPanel/DialogTitle").GetComponent<Text>();
        mDialogMessage = transform.Find("DialogPanel/DialogMessage").GetComponent<Text>();
        mButtonOK = transform.Find("DialogPanel/ButtonOK").GetComponent<Button>();
        mButtonCancel = transform.Find("DialogPanel/ButtonCancel").GetComponent<Button>();

    }

    // Use this for initialization
    void Start () {
        InitComponents();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetTitle(string title)
    {
        mDialogTitle.text = title;
    }

    public void ButItem(string currentProductID, Action<IPaymentCallbackMsg> successCallback, Action<IPaymentCallbackMsg
    > failCallback)
    {
        InitComponents();
        mDialogMessage.text = mDefaultMsg.Replace(@"{@@purchase_id}", currentProductID);
        t_successCallback = successCallback;
        t_failCallback = failCallback;
        mButtonOK.onClick.AddListener(() =>
        {
            t_successCallback(new PaymentCallbackMsg(""));
            Destroy(this.gameObject);
        });
        mButtonCancel.onClick.AddListener(() =>
        {
            t_failCallback(new PaymentCallbackMsg(""));
            Destroy(this.gameObject);
        });
    }
}
