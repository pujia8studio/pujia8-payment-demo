﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
	public interface IPaymentCallbackMsg
	{
		string GetMsg();

	}


	public class PaymentCallbackMsg : IPaymentCallbackMsg
	{
		string tMsg = String.Empty;

		public PaymentCallbackMsg()
		{
			
		}
		public PaymentCallbackMsg(string msg)
		{
			tMsg = msg;
		}
		public string GetMsg()
		{
			return tMsg;
		}
	}
}
