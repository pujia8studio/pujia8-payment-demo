﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{

    [DisallowMultipleComponent]
    public class GameworkPaymentPCStubMgr : MonoBehaviour, IStoreCallback, IPaymentMgr
    {
#region singleton
        private static GameworkPaymentPCStubMgr s_instance;
        public static GameworkPaymentPCStubMgr Instance
        {
            get
            {
                if (s_instance == null)
                {
                    return new GameObject("GameworkPaymentAndroidMgr").AddComponent<GameworkPaymentPCStubMgr>();
                }
                else
                {
                    return s_instance;
                }
            }
        }
        private bool m_bInitialized;
        public static bool Initialized
        {
            get
            {
                return Instance.m_bInitialized;
            }
        }
        private void Awake()
        {
            if (s_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            s_instance = this;
            DontDestroyOnLoad(gameObject);
            m_bInitialized = true;
        }

        private void OnEnable()
        {
            if (s_instance == null)
            {
                s_instance = this;
            }

            if (!m_bInitialized)
            {
                return;
            }
        }

        private void OnDestroy()
        {
            if (s_instance != this)
            {
                return;
            }

            s_instance = null;

            if (!m_bInitialized)
            {
                return;
            }
        }

        private void Update()
        {
            if (!m_bInitialized)
            {
                return;
            }
        }

        public void OnProcessPurchaseCallback(string callback)
        {
            throw new NotImplementedException();
        }

        public void OnPurchaseFailedCallback(string callback)
        {
            throw new NotImplementedException();
        }

        public void OnRestoreCallback(string callback)
        {
            throw new NotImplementedException();
        }

        public void OnRequestPurchaseCallback(string callback)
        {
            throw new NotImplementedException();
        }

        public void OnPriceGetCallback(string callback)
        {
            throw new NotImplementedException();
        }
        #endregion

        private Action onInitAction;
        public void Init(Action onInitCallback)
        {
            if (m_bInitialized)
            {
                return;
            }
            m_bInitialized = true;
            onInitAction = onInitCallback;
            Debug.LogWarning("GameworkPaymentPCStubMgr::Init()");
            if (onInitAction != null)
            onInitAction.Invoke();
        }

        public void AddProducts(GameStoreProduct product)
        {
            if (!GameworkPaymentMgr.Instance.ProductDictionary.ContainsKey(product.ProductID))
            {
                Debug.Log("[GameworkPaymentPCStubMgr]add product " + product.ProductID);
                GameworkPaymentMgr.Instance.ProductDictionary.Add(product.ProductID, product);
            }
        }

        public void SetEnvironment()
        {

        }

        public void BuyConsumable(GameStorePurchaserOptions options)
        {
            consumableDelegate = options.consumableDelegate;
            nonconsumableDelegate = options.nonconsumableDelegate;
            failDelegate = options.failDelegate;
            BuyProduct(options.product);
        }


        private void BuyProduct(string curProductID)
        {

            if (GetProductDictionary().ContainsKey(curProductID))
            {
                Debug.Log("product found :" + curProductID);
                GameStoreProduct product = GetProductDictionary()[curProductID];
                if (product.Consumable)
                {
                    if (consumableDelegate != null)
                    {
                        Debug.Log("consumableDelegate found :" + curProductID);

                        FakeStoreDialog cFakeDialog = Instantiate<FakeStoreDialog>(Resources.Load<FakeStoreDialog>("UI/_Gameworks_FakeStoreDialog"));
                        cFakeDialog.gameObject.transform.SetParent(this.gameObject.transform);
                        cFakeDialog.ButItem(curProductID, consumableDelegate, failDelegate);
                    }
                }
                else
                {
                    if (nonconsumableDelegate != null)
                    {
                        Debug.Log("nonconsumableDelegate found :" + curProductID);
                        FakeStoreDialog cFakeDialog = Instantiate<FakeStoreDialog>(Resources.Load<FakeStoreDialog>("UI/_Gameworks_FakeStoreDialog"));
                        cFakeDialog.gameObject.transform.SetParent(this.gameObject.transform);
                        cFakeDialog.ButItem(curProductID, nonconsumableDelegate, failDelegate);
                    }
                        
                }
            }
            else
            {
                string callbackStr = "product not found";
                ShowSystemAlert("错误", "暂时无法购买指定的商品");
                if (failDelegate != null)
                    failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                consumableDelegate = null;
                nonconsumableDelegate = null;
                failDelegate = null;
                return;
            }
            consumableDelegate = null;
            nonconsumableDelegate = null;
            failDelegate = null;
            return;

        }

        public void RestorePurchases(Action<string[]> p)
        {
            restoreDelegate = null;
            Debug.LogWarning("RestorePurchasesore ");
            restoreDelegate = p;
            restoreDelegate.Invoke(new string[] {});
        }

        public void GetPriceTable(Action<string> onPriceTableGetcallback)
        {
            onPriceTableGetcallback.Invoke("{}");
        }

        public void ShowSystemAlert(string title, string desc)
        {
            Debug.Log(title + ":" + desc);
        }

        /// <summary>
        /// Enable sandbox mode for Alipay 
        /// </summary>
        /// <param name="actived"></param>

        public void EnablePaymentSandbox(bool actived)
        {
            Debug.LogFormat("Sandbox enabled :{0}", actived);
        }


        Action<string> onPriceTableGet;
        Action<IPaymentCallbackMsg> consumableDelegate;
        Action<IPaymentCallbackMsg> nonconsumableDelegate;
        Action<IPaymentCallbackMsg> failDelegate;
        Action<string[]> restoreDelegate;

        public Dictionary<string, GameStoreProduct> GetProductDictionary()
        {
            return GameworkPaymentMgr.Instance.ProductDictionary;
        }
    }
}