﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
    public interface IStoreCallback
    {
        void OnProcessPurchaseCallback(string callback);
        void OnPurchaseFailedCallback(string callback);
        void OnRestoreCallback(string callback);
        void OnRequestPurchaseCallback(string callback);
        void OnPriceGetCallback(string callback);
    }

}
