﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
    public interface IPaymentMgr
    {
        void Init(Action onInitCallback);
        void AddProducts(GameStoreProduct product);
        void SetEnvironment();
        void BuyConsumable(GameStorePurchaserOptions option);
        void RestorePurchases(Action<string[]> p);
        void EnablePaymentSandbox(bool value);
        void GetPriceTable(Action<string> onPriceTableGetcallback);
        Dictionary<string, GameStoreProduct> GetProductDictionary();


    }

}

