﻿#if UNITY_IOS
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

namespace com.moogle.gameworks
{

    [DisallowMultipleComponent]
    public class GameworkPaymentIOSMgr : MonoBehaviour, IStoreCallback, IPaymentMgr
    {
#region singleton
        private static GameworkPaymentIOSMgr s_instance;
        public static GameworkPaymentIOSMgr Instance
        {
            get
            {
                if (s_instance == null)
                {
                    return new GameObject("GameworkPaymentAndroidMgr").AddComponent<GameworkPaymentIOSMgr>();
                }
                else
                {
                    return s_instance;
                }
            }
        }
        private bool m_bInitialized;
        public static bool Initialized
        {
            get
            {
                return Instance.m_bInitialized;
            }
        }
        private void Awake()
        {
            if (s_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            s_instance = this;
            DontDestroyOnLoad(gameObject);
            m_bInitialized = true;
        }

        private void OnEnable()
        {
            if (s_instance == null)
            {
                s_instance = this;
            }

            if (!m_bInitialized)
            {
                return;
            }
        }

        private void OnDestroy()
        {
            if (s_instance != this)
            {
                return;
            }

            s_instance = null;

            if (!m_bInitialized)
            {
                return;
            }
        }

        private void Update()
        {
            if (!m_bInitialized)
            {
                return;
            }
        }
#endregion

        
        private bool isInit = false;
        private bool isEnvSet = false;
        private static IStoreController m_StoreController;
        private static IExtensionProvider m_StoreExtensionProvider;

        public static IStoreController StoreController
        {
            get { return m_StoreController; }
        }

        public static IExtensionProvider StoreExtensionProvider
        {
            get
            {
                return m_StoreExtensionProvider;
            }
            
        }


        ConfigurationBuilder builder;
        IStoreListener pStoreListener;
        private Action onInitAction;
        public void Init(Action onInitCallback)
        {
            if (isInit){
                return;
            }

            onInitAction = onInitCallback;
            pStoreListener = GetComponent<PaymentIOSListener>();
            if (pStoreListener == null)
            {
                pStoreListener = gameObject.AddComponent<PaymentIOSListener>();
            }
            StandardPurchasingModule module = StandardPurchasingModule.Instance();
            if (Application.isEditor)
            {
                module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
                module.useFakeStoreAlways = true;
            }
            else
            {
                module.useFakeStoreAlways = false;
            }
            builder = ConfigurationBuilder.Instance(module);
            isInit = true;
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            m_StoreController = controller;
            m_StoreExtensionProvider = extensions;
            if (onInitAction != null)
                onInitAction.Invoke();

        }

        public void AddProducts(GameStoreProduct product)
        {
            if (builder != null)
            {
                if (!GetProductDictionary().ContainsKey(product.ProductID))
                {
                    GetProductDictionary().Add(product.ProductID, product);
                    switch (product.Consumable)
                    {
                        case true:
                            builder.AddProduct(product.ProductID, ProductType.Consumable, new IDs() {
                            { product.ProductID,  AppleAppStore.Name },
                        });
                            break;
                        case false:
                            builder.AddProduct(product.ProductID, ProductType.NonConsumable, new IDs() {
                            { product.ProductID,  AppleAppStore.Name },
                        });
                            break;
                        default:
                            builder.AddProduct(product.ProductID, ProductType.Consumable, new IDs() {
                            { product.ProductID,  AppleAppStore.Name },
                        });
                            break;
                    }
                }

            }
        }
        /*
         * 必须先AddProducts 添加所有的商品，再SetEnvironment()
         */

        public void SetEnvironment()
        {
            pStoreListener = GetComponent<PaymentIOSListener>();
            if (pStoreListener == null)
            {
                pStoreListener = gameObject.AddComponent<PaymentIOSListener>();
            }
            UnityPurchasing.Initialize(pStoreListener, builder);

        }

        private bool IsIAPInitialized()
        {
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }

        public string GetErrorReason()
        {
            return ((PaymentIOSListener) pStoreListener).GetErrorReason();
        }

        public void GetPriceTable(Action<string> oncallback)
        {
            if (!IsIAPInitialized())
            {
                Debug.LogError("GetPriceTable: IsIAPInitialized=false ,return");
                return;
            }
            Dictionary<string, GProductPrice> priceTable = new Dictionary<string, GProductPrice>();
            string text = "{}";
            try
            {
                Product[] products = m_StoreController.products.all;
                foreach (var curProduct in products)
                {
                    GProductPrice priceStruct = new GProductPrice
                    {
                        ProductID = curProduct.definition.id,
                        LocalizedPrice = curProduct.metadata.localizedPriceString,
                        Price = curProduct.metadata.localizedPrice.ToString(),
                        CurrencyType = curProduct.metadata.isoCurrencyCode
                    };
                    if (!priceTable.ContainsKey(curProduct.definition.id) &&
                        GameworkPaymentMgr.Instance.ProductDictionary.ContainsKey(priceStruct.ProductID))
                    {
                        priceTable.Add(curProduct.definition.id, priceStruct);
                    }
                }
                text = FrameworkUtils.JsonSerialize(priceTable);
            }
            catch (Exception e)
            {
                text = "{}";
                Debug.LogError("Got error while reading price table:" + e.Message + "\n" + e.StackTrace);
            }
            onPriceTableGet = oncallback;
            OnPriceGetCallback(text);

        }

        public void BuyConsumable(GameStorePurchaserOptions options)
        {

            if (isPurchasing == true) return;
            isPurchasing = true;
            consumableDelegate = null;
            nonconsumableDelegate = null;
            failDelegate = null;
            consumableDelegate = options.consumableDelegate;
            nonconsumableDelegate = options.nonconsumableDelegate;
            failDelegate = options.failDelegate;
            BuyProduct(options.product);

        }

        private void BuyProduct(string curProductID)
        {
            restoreFlag = 0;
            restoreCompleteFlag = 0;
            m_currentPurchaseId = curProductID;
            Debug.Log("Purchaser BuyProductID " + m_currentPurchaseId);
            try
            {
                if (IsIAPInitialized())
                {
                    Product product = m_StoreController.products.WithID(m_currentPurchaseId);
                    Debug.LogFormat("product={0}\n product.availableToPurchase={1}",curProductID ,product.availableToPurchase);
                    if (product != null && product.availableToPurchase)
                    {
                        Debug.Log(string.Format("Purchasing product asychronously: '{0}' - '{1}'", product.definition.id, product.definition.storeSpecificId));
                        m_StoreController.InitiatePurchase(product);
                        return;
                    }
                    // Otherwise ...
                    else
                    {
                        Debug.LogFormat("BuyProductID: {0}FAIL. Not purchasing product, either is not found or is not available for purchase", curProductID);
                        OnPurchaseFailedCallback("Item not avaliable for purchase in your store");
                        return;
                        
                    }
                }
                else
                {
                    Debug.Log("BuyProductID FAIL. Not initialized.");
                    OnPurchaseFailedCallback(GetErrorReason());
                    return;
                }
            }
            catch (Exception e)
            {
                Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);

            }

            OnPurchaseFailedCallback("BuyProductID: FAIL");

        }
        int restoreFlag = 0;
        public int RestoreFlag
        {
            get { return restoreFlag; }
        }
        int restoreCompleteFlag = 0;
        public int RestoreCompleteFlag
        {
            get { return restoreCompleteFlag; }
        }
        
        public List<string> RestoreList = new List<string>();
        public void RestorePurchases(Action<string[]> p)
        {
            RestoreList.Clear();
            restoreFlag = 1;
            restoreCompleteFlag = 0;
            restoreDelegate = p;
            if (!IsIAPInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                OnRestoreCallback("");
                return;
            }
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                Debug.Log("RestorePurchases started ...");
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                apple.RestoreTransactions((result) =>
                {
                    restoreCompleteFlag = 1;
                    OnRestoreCallback(string.Join(",", RestoreList.ToArray()));
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                    if (!result)
                    {
                        OnRestoreCallback("{}");
                    }
                });
            }
            else
            {
                Debug.Log("Not iOS ...");
            }
        }
        

        public void EnablePaymentSandbox(bool actived)
        {
        }


        public Dictionary<string, GameStoreProduct> GetProductDictionary()
        {
            return GameworkPaymentMgr.Instance.ProductDictionary;
        }


        public void OnProcessPurchaseCallback(string callbackStr)
        {
            isPurchasing = false;
            Debug.LogWarning("OnProcessPurchaseCallback" + callbackStr);
            //回调购买数据
            //数据格式 productid,result_status
            if (callbackStr.Contains(","))
            {
                string[] param = callbackStr.Split(","[0]);
                string temp_productid = param[0];
                string temp_status = param[1];
                string temp_tradeno = param[2];
                if (temp_status == GamepayConst.RESULT_CODE_SUCCESS && GetProductDictionary().ContainsKey(temp_productid))
                {
                    GameStoreProduct tempProduct = GetProductDictionary()[temp_productid];
                    if (restoreFlag == 0)
                    {
                        if (GameworkPaymentMgr.PriceDict.ContainsKey(temp_productid))
                        {
                            Debug.Log("Execute Record Purchase " + temp_productid);
                            this.RecordPurchaseSuccessOnly(temp_tradeno, temp_productid, GameworkPaymentMgr.PriceDict[temp_productid].Price,
                                GameworkPaymentMgr.PriceDict[temp_productid].CurrencyType, "appstore");
                            //RecordPurchaseSuccess(temp_tradeno, temp_productid);
                        }
                        else
                        {
                            Debug.Log("Not found in PriceDict :" + temp_productid);
                        }
                    }

                    if (tempProduct.Consumable)
                    {
                        Debug.LogWarning("consumableDelegate" + callbackStr);
                        if (consumableDelegate != null)
                        {
                            consumableDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                            return;
                        }
                    }
                    else
                    {
                        Debug.LogWarning("nonconsumableDelegate" + callbackStr);
                        if (nonconsumableDelegate != null)
                        {
                            nonconsumableDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                            return;
                        }
                            
                    }
                }
                else
                {
                    if (failDelegate != null)
                    {
                        failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                        return;
                    }   
                }
            }
            else
            {
                if (failDelegate != null)
                    failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
                return;
            }
            if (failDelegate != null)
                failDelegate.Invoke(new PaymentCallbackMsg(callbackStr));
            return;
        }

        public void OnPurchaseFailedCallback(string callback)
        {
            isPurchasing = false;
            Debug.LogWarning("OnPurchaseFailedCallback");
            if (failDelegate != null)
                failDelegate.Invoke(new PaymentCallbackMsg(callback));
        }

        public void OnRestoreCallback(string callback)
        {
            Debug.LogWarning("OnRestoreCallback" + callback);
            var result = callback.Split(',');
            if (restoreDelegate != null)
            {
                restoreDelegate.Invoke(result);
            }
        }

        public void OnRequestPurchaseCallback(string callback)
        {
            Debug.LogWarning("OnRequestPurchaseCallback" + callback);
        }
        public void OnPriceGetCallback(string callback)
        {
            Debug.LogWarning("OnPriceGetCallback :" + callback);
            if (callback.Contains("{") && callback.Contains("}"))
            {
                try
                {
                    GameworkPaymentMgr.PriceDict = FrameworkUtils.JsonDeserialize<Dictionary<string, GProductPrice>>(callback);
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message.ToString());
                }
            }
            if (onPriceTableGet != null) onPriceTableGet.Invoke(callback);
        }

        public void RecordPurchaseRequest(string tradeNo, string purchaseid, string price_yuan, string currencyType, string tradeType)
        {
            float price = 0f;
            float.TryParse(price_yuan, out price);
            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseRequest(tradeNo, purchaseid, price, currencyType, tradeType);
        }
        
        public void RecordPurchaseSuccess(string tradeNo, string purchaseid)
        {
            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseSuccess(tradeNo, purchaseid);
        }
        
        public void RecordPurchaseFail(string tradeNo, string purchaseid)
        {
            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseFail(tradeNo, purchaseid);
        }
        
        public void RecordPurchaseSuccessOnly(string tradeNo, string purchaseid, string price_yuan, string currencyType, string tradeType)
        {
            float price = 0f;
            float.TryParse(price_yuan, out price);
            GameworksTrackerMgr.Instance.GetPlugin().OnRecordPurchaseSuccessOnly(tradeNo, purchaseid, price,
                currencyType, tradeType);
        }
        

        private bool isPurchasing = false;

        string m_currentPurchaseId = "";
        public String CurrentPurchaseId
        {
            get
            {
                return m_currentPurchaseId;
            }
        }
        Action<string> onPriceTableGet;
        Action<IPaymentCallbackMsg> consumableDelegate;
        Action<IPaymentCallbackMsg> nonconsumableDelegate;
        Action<IPaymentCallbackMsg> failDelegate;
        Action<string[]> restoreDelegate;
    }


}
#endif