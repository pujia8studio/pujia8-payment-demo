﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
	public class GProductPrice
	{
		//商品ID
		public string ProductID;

		//价格，纯数字没有货币单位，可以转换为float
		public string Price;
		
		//本地化价格,用于显示，仅供UI使用
		public string LocalizedPrice;

		//货币类型
		public string CurrencyType;

		public GProductPrice()
		{
			
		}

		public GProductPrice(string productId, string price, string currencyType)
		{
			ProductID = productId;
			Price = price;
			LocalizedPrice = price;
			CurrencyType = currencyType;
		}

		public static GProductPrice GetDefaultPrice()
		{
			return new GProductPrice("null", "0", "CNY");
		}

		public static Dictionary<string, GProductPrice> ParseJsonData(string value)
		{
			Dictionary<string, GProductPrice> dst = new Dictionary<string, GProductPrice>();
			try
			{
				dst = FrameworkUtils.JsonDeserialize<Dictionary<string, GProductPrice>>(value);
			}
			catch (Exception e)
			{
				Debug.LogError(e.StackTrace);
			}

			return dst;
		}
		
	}

}
