﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{

    public abstract class GameworksTrackerTemplate : MonoBehaviour
    {
        public virtual void Init()
        {

        }

        public virtual void SetChargeRecordEnabled(bool value)
        {

        }

        public virtual void RecordUser()
        {

        }

        public virtual void RecordServer(string serverName)
        {

        }

        /// <summary>
        /// 发起充值请求
        /// </summary>
        /// <param name="orderId">订单ID</param>
        /// <param name="productId">商品id</param>
        /// <param name="price">价格，元</param>
        /// <param name="paymentMethod">支付方式</param>
        public virtual void OnRecordPurchaseRequest(string orderId, string productId, float price, string currencyType, string paymentMethod)
        {

        }
        /// <summary>
        /// 充值成功时调用，需要与充值请求成对调用
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="productId"></param>
        public virtual void OnRecordPurchaseSuccess(string orderId, string productId)
        {

        }
        /// <summary>
        /// 充值失败时调用，需要与充值请求成对调用
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="productId"></param>
        public virtual void OnRecordPurchaseFail(string orderId, string productId)
        {

        }
        /// <summary>
        /// 仅充值成功
        /// 当客户端无法跟踪充值请求发起，只能跟踪到充值成功的事件时，调用该接口记录充值信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="productId"></param>
        /// <param name="price"></param>
        /// <param name="paymentMethod"></param>
        public virtual void OnRecordPurchaseSuccessOnly(string orderId, string productId, float price,
            string currencyType, string paymentMethod)
        {

        }




    }
}
