﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{

    public class GameworksTrackerTapDB : GameworksTrackerTemplate
    {
        private bool m_bInitialized = false;
        private bool enableChargeRecord = true;

        private void Start()
        {
            Init();
        }

        private void OnDestroy()
        {
            if (Application.isEditor || Application.platform != RuntimePlatform.Android)
            {
                return;
            }
            TapDB.onStop();
        }

        public override void Init()
        {
#if UNITY_ANDROID
            InitAndroid();
#endif
#if UNITY_IOS
            InitiOS();
#endif
        }

        public override void SetChargeRecordEnabled(bool value)
        {
            enableChargeRecord = value;
        }
        
        public override void RecordUser()
        {
#if UNITY_ANDROID
            RecordUser_Android();
#endif
#if UNITY_IOS
            RecordUser_iOS();
#endif
        }

        public override void RecordServer(string subChannelID)
        {
#if UNITY_ANDROID
            RecordServer_Android(subChannelID);
#endif
#if UNITY_IOS
            RecordServer_iOS(subChannelID);
#endif
        }

        private QTapdbConfig mConfig;
#if UNITY_IOS
        private void InitiOS()
        {
            if (m_bInitialized)
            {
                return;
            }
            if (Application.isEditor || Application.platform != RuntimePlatform.IPhonePlayer)
            {
                return;
            }
            m_bInitialized = true;
            mConfig = new QTapdbConfig();
            TextAsset configfile = Resources.Load<TextAsset>("iOSConfig/TapdbConf");
            mConfig.ReadConfig(configfile.text);
            TapDB.onStart(mConfig.Config.TapDB_Tracker_AppID, mConfig.Config.TapDB_Tracker_ChannelID, mConfig.Config.GameVersion_ID, true);
            RecordUser();
            RecordServer(mConfig.Config.TapDB_Tracker_ChannelID);
        }

        private void RecordUser_iOS()
        {
            string random_id = FrameworkUtils.GetRandomUserId();
            TapDB.setUser(random_id, TGTUserType.TGTTypeRegistered, TGTUserSex.TGTSexMale, 0, random_id);
        }

        private void RecordServer_iOS(string subChannelID)
        {
            TapDB.setServer(subChannelID);
        }
#endif
#if UNITY_ANDROID
        private void InitAndroid()
        {
            if (m_bInitialized)
            {
                return;
            }
            if (Application.isEditor || Application.platform != RuntimePlatform.Android)
            {
                return;
            }
            m_bInitialized = true;
            
            mConfig = new QTapdbConfig();
            string jsondata = GWCoreUtils.ReadTextFileFromAssets("TapdbConf.json");
            mConfig.ReadConfig(jsondata);
            TapDB.onStart(mConfig.Config.TapDB_Tracker_AppID, mConfig.Config.TapDB_Tracker_ChannelID, GWCoreUtils.GetAppVersion(), true);
            RecordUser();
            RecordServer(mConfig.Config.TapDB_Tracker_ChannelID);

        }

        private void RecordUser_Android()
        {
            string random_id = "android" + GWCoreUtils. GetDeviceCRC();
            TapDB.setUser(random_id, TGTUserType.TGTTypeRegistered, TGTUserSex.TGTSexMale, 0, random_id);
        }

        private void RecordServer_Android(string subChannelID)
        {
            TapDB.setServer(subChannelID);
        }
#endif


        /// <summary>
        /// 发起充值请求
        /// </summary>
        /// <param name="orderId">订单ID</param>
        /// <param name="productId">商品id</param>
        /// <param name="price">价格，元</param>
        /// <param name="paymentMethod">支付方式</param>
        public override void OnRecordPurchaseRequest(string orderId, string productId, float price, string currencyType, string paymentMethod)
        {
            if (enableChargeRecord == false || m_bInitialized == false)
            {
                return;
            }
            if (Application.isEditor || ((Application.platform != RuntimePlatform.Android) && (Application.platform != RuntimePlatform.IPhonePlayer)))
            {
                
                return;
            }
            Debug.LogWarning("Tapdb::OnRecordPurchaseRequest()");
            TapDB.onChargeRequest(orderId, productId, (Int32)(price * 100f), currencyType, 1, paymentMethod);
        }
        /// <summary>
        /// 充值成功时调用，需要与充值请求成对调用
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="productId"></param>
        public override void OnRecordPurchaseSuccess(string orderId, string productId)
        {
            if (enableChargeRecord == false || m_bInitialized == false)
            {
                return;
            }
            if (Application.isEditor || ((Application.platform != RuntimePlatform.Android) && (Application.platform != RuntimePlatform.IPhonePlayer)))
            {
                
                return;
            }
            Debug.LogWarning("Tapdb::OnRecordPurchaseSuccsee()");
            TapDB.onChargeSuccess(orderId);
        }
        /// <summary>
        /// 充值失败时调用，需要与充值请求成对调用
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="productId"></param>
        public override void OnRecordPurchaseFail(string orderId, string productId)
        {
            if (enableChargeRecord == false || m_bInitialized == false)
            {
                return;
            }
            if (Application.isEditor || ((Application.platform != RuntimePlatform.Android) && (Application.platform != RuntimePlatform.IPhonePlayer)))
            {
                
                return;
            }
            Debug.LogWarning("Tapdb::OnRecordPurchaseFail()");
            TapDB.onChargeFail(orderId, "Fail");
        }
        /// <summary>
        /// 仅充值成功
        /// 当客户端无法跟踪充值请求发起，只能跟踪到充值成功的事件时，调用该接口记录充值信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="productId"></param>
        /// <param name="price">价格，元</param>
        /// <param name="paymentMethod"></param>
        public override void OnRecordPurchaseSuccessOnly(string orderId, string productId, float price,
            string currencyType, string paymentMethod)
        {
            if (enableChargeRecord == false || m_bInitialized == false)
            {
                return;
            }
            if (Application.isEditor || ((Application.platform != RuntimePlatform.Android) && (Application.platform != RuntimePlatform.IPhonePlayer)))
            {
                
                return;
            }
            Debug.LogWarning("Tapdb::OnRecordPurchaseSuccessOnly()");
            TapDB.onChargeOnlySuccess(orderId, productId, (Int32)(price * 100f), currencyType, 1, paymentMethod);
        }

    }

}