using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
    
    public class QTapdbConfig
    {
        public QConfigFile Config;

        public void ReadConfig(string json)
        {
            QConfigFile curConfig = QTapdbConfig.Load(json);
            Config = curConfig;
        }

        public static QConfigFile Load(string json)
        {
            if (json.Contains("{") && json.Contains("}"))
            {
                Debug.Log("Load QConfigFile... ok");
                return FrameworkUtils.JsonDeserialize<QConfigFile>(json);
            }
            Debug.Log("Load QConfigFile... failed");
            return new QConfigFile();
        }

        [Serializable]
        public class QConfigFile
        {
            public string TapDB_Tracker_AppID;
            public string TapDB_Tracker_ChannelID;
            public string GameVersion_ID;
        }
    }
}