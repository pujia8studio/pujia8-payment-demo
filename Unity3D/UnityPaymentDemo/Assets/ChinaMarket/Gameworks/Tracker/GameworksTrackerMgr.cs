﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
    public class GameworksTrackerMgr : SingletonMono<GameworksTrackerMgr>
    {

        private GameworksTrackerTemplate template;
        private bool isInit = false;
        public void Init()
        {
            if (isInit)
            {
                return;
            }
            isInit = true;
            gameObject.name = "GameworksTrackerMgr";
            if (template == null)
            {
                template = GetPlugin();
                template.Init();
            }

        }

        public GameworksTrackerTemplate GetPlugin()
        {
            template = gameObject.GetComponent<GameworksTrackerTapDB>();
            if (template == null)
            {
                template = gameObject.AddComponent<GameworksTrackerTapDB>(); //接入tapdb
            }
            return template;
        }

    }

}
