﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BuildPlayer {
#if UNITY_5_6_OR_NEWER && UNITY_ANDROID
    [MenuItem("Assets/Pujia8 Gameworks/Android Build/Export Android Project(Release IL2CPP)")]
	public static void ExportProjectData()
	{
	    if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
	    {
	        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
	    }
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
        EditorUserBuildSettings.exportAsGoogleAndroidProject = true;
        BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, "export_project", BuildTarget.Android, 
								BuildOptions.AcceptExternalModificationsToPlayer 
                                | BuildOptions.CompressWithLz4
                                | BuildOptions.Il2CPP
                                );
        Debug.Log("[Android Build System]Build complete.");
	}
#endif
}
