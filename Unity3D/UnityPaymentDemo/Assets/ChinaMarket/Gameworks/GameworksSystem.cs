﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.moogle.gameworks
{
    public class GameworksSystem
    {
        public static void InitAllSystem()
        {
            GameworksTrackerMgr.Instance.Init();
#if UNITY_IOS
            GameworksTrackerMgr.Instance.GetPlugin().SetChargeRecordEnabled(true);
#elif UNITY_ANDROID
            GameworksTrackerMgr.Instance.GetPlugin().SetChargeRecordEnabled(false);
#endif
        }
    }

}
