import codecs
import json
import struct
import base64
import json

class JsonHelper(object):
    @staticmethod
    def Serialize(obj_dict):
        return json.dumps(obj_dict, default=lambda o: o.__dict__, sort_keys=True ,ensure_ascii=False,indent=4)
        
    @staticmethod
    def Deserialize(str_json_data):
        if (str_json_data[0] == u"\uFEFF"):
            str_json_data = str_json_data[1:]
        return json.loads(str_json_data)

class AppIapConfig(object):
    def __init__(self):
        self.LCKey = ""
        self.Items = {}
        pass

    
class ProductType(object):
    def __init__(self):
        self.Type = "consumable"
        self.SKU = "sku"
        self.ProductID = "productID"
        pass


publickeyFileName = "google-play-publickey.txt"
productFileName = "product-ids.csv"
enc_key = "mofumofuhogehoge"
def ReadProductID(filename):
    productList = []
    lines = codecs.open(filename, "rb", "utf-8").readlines()
    if (len(lines) > 1):
        for i in xrange(1, len(lines)):
            line = (lines[i].replace("\r", "")).replace("\n", "")
            if (line.split(",") >= 3):
                vals = line.split(",")
                ptype = ProductType()
                if (vals[2] == "true"):
                    ptype.Type = "consumable"
                elif (vals[2] == "false"):
                    print(("non-consumable"))
                    ptype.Type = "non-consumable"
                else:
                    ptype.Type = "consumable"
                ptype.SKU = vals[1]
                ptype.ProductID = vals[0]
                productList.append(ptype)
            pass
    return productList

def ReadPublicKey(filename):
    fs = open(filename, "rb")
    key = fs.read()
    key = key.replace("\r", "")
    key = key.replace("\t", "")
    key = key.replace("\n", "")
    key = key.replace(" ", "")
    return key

def EncData():
    key = ReadPublicKey(publickeyFileName)
    products = ReadProductID(productFileName)
    key_len = len(key)
    dst = open("gpc.bin", "wb")
    iapConfig = AppIapConfig()
    iapConfig.LCKey = key
    iapConfig.Items = {}
    for mp in products:
        iapConfig.Items[mp.ProductID] = mp
    dumps = JsonHelper.Serialize(iapConfig)
    encstr = base64.encodestring(dumps)
    encstr = (encstr.replace("\r", "")).replace("\n", "")
    dst.write(encstr)
    dst.close()
    pass

if __name__ == "__main__":
    EncData()