package com.moogle.paymentdemo;

import android.app.Application;

import com.moogle.gameworks_payment_java.Elysium;


//in Corona SDK, you may write in CoronaApplication directly
//or extends CoronaApplication then override the onCreate() method.
//with Unity3D Engine ,just create  public class MainApplication extends Application{}, and copy codes
public class DemoMainApplication
        extends Application {
    @Override
    public void onCreate()
    {
        super.onCreate();
        // add following code and add the DemoMainApplication to AndroidManifest.xml.
        // example:
        //<application .......android:name="com.moogle.paymentdemo.DemoMainApplication">

        Elysium.onApplicationCreate(this);//
    }

}