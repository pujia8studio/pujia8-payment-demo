package com.moogle.paymentdemo;

/**
 * test Purchase Info, just for demo test
 */

public class PurchaseInfo
{
    public final static String PurchaseID = "test_pay_01";
    public final static String ItemName = "IAPTEST01";
    public final static String ItemDesc = "IAPTEST01";
    public final static String ItemPrice = "0.01";
    public final static boolean IsConsumable = true;


    public final static String PurchaseID3 = "test_pay_03";
    public final static String ItemName3 = "IAPTEST03";
    public final static String ItemDesc3 = "IAPTEST03";
    public final static String ItemPrice3 = "0.01";
    public final static boolean IsConsumable3 = false;

}
