package com.moogle.paymentdemo;

import android.app.Activity;
import android.app.Fragment;
import android.util.Log;

import com.moogle.gameworks_payment_java.ChannelStoreManager;
import com.moogle.gameworks_payment_java.FrameworkConsts;
import com.moogle.gameworks_payment_java.IPaymentCallback;
import com.moogle.gameworks_payment_java.IPaymentRequest;
import com.moogle.gameworks_payment_java.payment.BasePaymentController;


public class IAPFragment extends Fragment implements IPaymentCallback,IPaymentRequest
{
    public final static String DEBUG_TAG = "IAPFragment";
    public boolean isManagerInitialized = false;
    private static IAPFragment Instance;
    private Activity mActivity;
    private ChannelStoreManager mChannelStoreManager;
    private BasePaymentController paymentController;
    private IUIMsgSender uiMsgSender;

    public static IAPFragment GetInstance()
    {
        if(Instance == null)
        {
            Instance = new IAPFragment();
            Log.d(DEBUG_TAG, "init Instance");
        }
        return Instance;
    }
    public void CreateFragment(Activity pActivity, IUIMsgSender pUiSender){
        if (isManagerInitialized == true) return;
        mActivity = pActivity;
        uiMsgSender = pUiSender;
        if (mActivity.getFragmentManager().findFragmentByTag(DEBUG_TAG) == null){
            mActivity.getFragmentManager().beginTransaction().add(Instance, DEBUG_TAG).commit();
        }
        else if (!mActivity.getFragmentManager().findFragmentByTag(DEBUG_TAG).isAdded())
        {
            mActivity.getFragmentManager().beginTransaction().add(Instance, DEBUG_TAG).commit();
        }
        mChannelStoreManager = ChannelStoreManager.GetInstance();
        mChannelStoreManager.Initalize(mActivity);
        this.Initalize(this);
        this.SetEnvironment();
        //Turn on sanbox mode for test
        //Elysium.EnablePaymentSandbox(true);

    }
    public boolean isLoaded()
    {
        return mChannelStoreManager.isPaymentEnabled;
    }


    @Override
    public void Initalize(IPaymentCallback pGameCallback) {
        mChannelStoreManager.GetPaymentController().Initalize(pGameCallback);

    }

    @Override
    public void SetEnvironment() {
        mChannelStoreManager.GetPaymentController().SetEnvironment();
    }



    @Override
    public void PurchaseItem(String purchaseID, String itemName, String itemDesc, boolean isConsumable) {

        //you will receive the result in OnProcessPurchaseCallback(string result)
        //if purchase fails ,see  OnPurchaseFailedCallback(string result);
        mChannelStoreManager.PurchaseItem(purchaseID,itemName,itemDesc,isConsumable);
    }

    @Override
    public void RestorePurchase() {

        //you will receive the result in OnRestoreCallback(string result)
        mChannelStoreManager.GetPaymentController() .RestorePurchase();
    }

    @Override
    public void SaveInventory() {
        //method is deprecated, there is no need to force store inventory
        // the inventory will save to server database automatically
        mChannelStoreManager.GetPaymentController().SaveInventory();
    }

    @Override
    public String GetFakeDeviceCRC() {
        //device identifier
        return mChannelStoreManager.GetPaymentController().GetFakeDeviceCRC();
    }

    public void RequestPrice(){

        mChannelStoreManager.GetPaymentController().GetPriceTable();
    }


    @Override
    public void OnProcessPurchaseCallback(String result) {
        SendMessageToUI(result);
        if (result.contains(",")){
            String[] param = result.split(",");
            String temp_productid = param[0];
            String temp_status = param[1];
            String temp_tradeno = param[2];
            if (temp_status.equals(FrameworkConsts.RESULT_CODE_SUCCESS)){
                SendMessageToUI("Purchase done!");

                ShowAlartDialog("Success","you have purchased " + temp_productid);
            }
            else{
                SendMessageToUI("Fail，Purchase FAILED" + temp_status);
            }
        }
        else{
            SendMessageToUI("Fail，Purchase FAILED");
        }
    }

    @Override
    public void OnPurchaseFailedCallback(String result) {
        //SendMessageToUI(result);
        SendMessageToUI("Fail，Purchase FAILED:\n" + result);
    }

    @Override
    public void OnRestoreCallback(String result) {
        SendUIClearview();
        //SendMessageToUI(result);
        SendMessageToUI("OnRestoreCallback:\n" + result);
    }

    @Override
    public void OnRequestPurchaseCallback(String result) {
        //SendMessageToUI(result);
        SendMessageToUI("OnRequestPurchaseCallback:\n" + result);
    }

    @Override
    public void OnPriceGetCallback(String result) {
        SendUIClearview();
        //SendMessageToUI(result);
        SendMessageToUI("OnPriceGetCallback:\n" + result);
    }

    public void ShowAlartDialog(String title, String desc){
        ChannelStoreManager.GetInstance().ShowAlartDialog(title, desc);
    }

    public void SendMessageToUI(final String result){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                uiMsgSender.Send(result);
            }
        });

    }

    public void SendUIClearview(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                uiMsgSender.Clear();
            }
        });

    }

}
