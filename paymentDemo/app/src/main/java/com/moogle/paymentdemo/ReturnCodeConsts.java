package com.moogle.paymentdemo;

/**
 * These are all callback return codes from server
 * RESULT_CODE_SUCCESS = "9000" means success.
 */

public class ReturnCodeConsts
{
    public final static String RESULT_CODE_SUCCESS = "9000";
    public final static String RESULT_CODE_PAYING = "8000";
    public final static String RESULT_CODE_FAIL = "4000";
    public final static String RESULT_CODE_CANCEL = "6001";
    public final static String RESULT_CODE_NETWORK_ERR = "6002";
    public final static String RESULT_CODE_UNKNOWN= "6004";
}
