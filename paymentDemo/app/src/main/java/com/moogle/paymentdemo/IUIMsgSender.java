package com.moogle.paymentdemo;

/**
 * test event dispatcher
 */

public interface IUIMsgSender {
    public void Send(String msg);
    public void Clear();
}
