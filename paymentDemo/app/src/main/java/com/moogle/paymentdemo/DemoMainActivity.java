package com.moogle.paymentdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.terasurware.nekobachi_cn.R;


//import me and Initalize me
import com.moogle.gameworks_payment_java.Elysium;



public class DemoMainActivity extends AppCompatActivity implements IUIMsgSender {

    private TextView mInfoView;
    private Button mBtn;
    private Button mBtnnon;
    private Button mReqPriceBtn;
    private Button mBtnReqRestore;
    private void InitComponents(){
        mInfoView = (TextView) findViewById(R.id.textViewInfo);
        mInfoView.setMovementMethod(ScrollingMovementMethod.getInstance());
        mBtn = (Button) findViewById(R.id.btnBuyItem);
        mReqPriceBtn = (Button) findViewById(R.id.btnReqPrice);
        mBtnReqRestore = (Button) findViewById(R.id.btnRestore);
        mBtnnon = (Button) findViewById(R.id.btnBuyNon);
        mBtn. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(DemoMainActivity.this, "Purchase Item！", Toast.LENGTH_SHORT).show();
                LogInfo("Purchase Item！");
                IAPFragment.GetInstance().PurchaseItem(PurchaseInfo.PurchaseID, PurchaseInfo.ItemName, PurchaseInfo.ItemDesc, PurchaseInfo.IsConsumable );
            }
        });
        mBtnnon. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(DemoMainActivity.this, "Purchase Nonconsumable Item！", Toast.LENGTH_SHORT).show();
                LogInfo("Purchase Nonconsumable Item！");
                IAPFragment.GetInstance().PurchaseItem(PurchaseInfo.PurchaseID3, PurchaseInfo.ItemName3, PurchaseInfo.ItemDesc3, PurchaseInfo.IsConsumable3 );
            }
        });
        mReqPriceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DemoMainActivity.this, "Request Price！", Toast.LENGTH_SHORT).show();
                LogInfo("Request Price");
                IAPFragment.GetInstance().RequestPrice();

            }
        });

        mBtnReqRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DemoMainActivity.this, "Request Restore！", Toast.LENGTH_SHORT).show();
                IAPFragment.GetInstance().RestorePurchase();
            }
        });

    }


    /**
     *
     * with Corona sdk ,you should extends com.ansca.corona.CoronaActivity and override the onCreate()
     * with Cocos2d-x ,you should extends AppActivity extends org.cocos2dx.lib.Cocos2dxActivity and override the onCreate()
                       like public class AppActivity extends Cocos2dxActivity
     * with Unity3D Engine ,you should  extends the UnityPlayerActivity and override the onCreate()
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_main);
        this.InitComponents();


        //( you must ) add following code under onCreate() for IAP init
        Elysium.Initalize(this);
        Elysium.onActivityCreate(this, savedInstanceState );
        //IAPFragment ,for test
        IAPFragment.GetInstance().CreateFragment(this,this);
    }



    @Override
    public void Send(String msg) {
        LogInfo(msg);
    }
    @Override
    public void Clear(){
        ClearView();
    }


    //( you should ) add following code under onDestroy.....onKeyDown()
    @Override
    protected void onDestroy(){
        Elysium.onActivityDestroy();
        super.onDestroy();

    }
    @Override
    protected void onNewIntent(Intent intent)
    {
        Elysium.onActivityNewIntent(intent);
        super.onNewIntent(intent);

    }
    @Override
    protected void onPause()
    {
        Elysium.onActivityPause();
        super.onPause();

    }
    @Override
    protected void onResume()
    {
        Elysium.onActivityResume();
        super.onResume();

    }
    @Override
    protected void onStart()
    {
        Elysium.onActivityStart();
        super.onStart();

    }
    @Override
    protected void onStop()
    {
        Elysium.onActivityStop();
        super.onStop();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { //按下的如果是BACK，同时没有重复
            return Elysium.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    private int offset;

    private void LogInfo(final String info){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mInfoView.append(info + '\n');
                offset = mInfoView.getLineCount() * mInfoView.getLineHeight();
                if(offset > mInfoView.getHeight()){
                    mInfoView.scrollTo(0,offset - mInfoView.getHeight());
                }
            }
        });
    }

    private void ClearView(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mInfoView.setText("");
                mInfoView.scrollTo(0,0);
            }
        });
    }

}
