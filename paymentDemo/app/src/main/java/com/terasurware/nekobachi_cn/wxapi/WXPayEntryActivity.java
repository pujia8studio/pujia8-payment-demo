package com.terasurware.nekobachi_cn.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.moogle.gameworks_payment_java.Elysium;
/**
 * For Wechat pay
 * (necessary)
 * copy .wxapi/WXPayEntryActivity.java to game_package_name/wxapi/WXPayEntryActivity.java
*
* */
public class WXPayEntryActivity extends Activity
{
    private static final String TAG = "WXPayEntryActivity";
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Elysium.WxpayOnEntryActivityCreate(this, getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Elysium.WxpayOnNewIntent(intent);
    }
}