import codecs
import json
import struct
import base64
import json

class JsonHelper(object):
    @staticmethod
    def Serialize(obj_dict):
        return json.dumps(obj_dict, default=lambda o: o.__dict__, sort_keys=True ,ensure_ascii=False,indent=4)
        
    @staticmethod
    def Deserialize(str_json_data):
        if (str_json_data[0] == u"\uFEFF"):
            str_json_data = str_json_data[1:]
        return json.loads(str_json_data)

class AppIapConfig(object):
    def __init__(self):
        self.APPID = ""
        self.LCKey = ""
        self.Items = {}
        pass

    
class ProductType(object):
    def __init__(self):
        self.Type = "consumable"
        self.SKU = "sku"
        self.ProductID = "productID"
        self.Price = "0.01"
        self.LocalizedPrice = "0.01"
        self.CurrencyType = "USD"
        pass


publickeyFileName = "qoo-configs.txt"
productFileName = "product-ids.csv"

def ReadProductID(filename):
    productList = []
    lines = codecs.open(filename, "rb", "utf-8").readlines()
    if (len(lines) > 1):
        for i in xrange(1, len(lines)):
            line = (lines[i].replace("\r", "")).replace("\n", "")
            if (line.split(",") >= 5):
                vals = line.split(",")
                ptype = ProductType()
                if (vals[2] == "true"):
                    ptype.Type = "consumable"
                elif (vals[2] == "false"):
                    print(("non-consumable"))
                    ptype.Type = "non-consumable"
                else:
                    ptype.Type = "consumable"
                ptype.SKU = vals[1]
                ptype.ProductID = vals[0]
                ptype.Price = vals[3]
                ptype.LocalizedPrice = vals[4]
                ptype.CurrencyType = vals[5]
                productList.append(ptype)
            pass
    return productList

def ReadAppSecret(filename):
    fs = open(filename, "rb")
    lines = fs.readlines()
    app_id = "0"
    app_secret = "0"
    for line in lines:
        if ("appid=" in line):
            val0 = line.split("appid=")[1]
            val0 = val0.replace(" ", "")
            val0 = val0.replace("\r", "")
            val0 = val0.replace("\n", "")
            val0 = val0.replace(",", "")
            app_id = val0
        if ("app_secret=" in line):
            val0 = line.split("app_secret=")[1]
            val0 = val0.replace(" ", "")
            val0 = val0.replace("\r", "")
            val0 = val0.replace("\n", "")
            val0 = val0.replace(",", "")
            app_secret = val0
    return (app_id, app_secret)

def EncData():
    (app_id, app_secret) = ReadAppSecret(publickeyFileName)
    products = ReadProductID(productFileName)
    dst = open("qoopc.bin", "wb")
    iapConfig = AppIapConfig()
    iapConfig.APPID = app_id
    iapConfig.LCKey = app_secret
    iapConfig.Items = {}
    for mp in products:
        iapConfig.Items[mp.ProductID] = mp
    dumps = JsonHelper.Serialize(iapConfig)
    encstr = base64.encodestring(dumps)
    encstr = (encstr.replace("\r", "")).replace("\n", "")
    dst.write(encstr)
    dst.close()
    pass

if __name__ == "__main__":
    EncData()